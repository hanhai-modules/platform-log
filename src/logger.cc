/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/logger.h"

#include <ctime>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>

#include "impl/log_impl.h"
#include "hanhai-platform-log/version/version.h"


namespace hanhai {
namespace log {

// Logger::Logger() {
//   // pass
// }

Logger::~Logger(){
  delete impl_;
  impl_ = nullptr;
}

bool Logger::Initialize(std::string const applicationId,
                        std::string const applicationDescription,
                        LogLevel const defaultLogLevel, LogMode const mode,
                        std::string const filePath) noexcept {
  if (impl::LoggerImpl::is_running_.load(std::memory_order_release)) {
    return false;
  }

  default_log_level_ = defaultLogLevel;
  impl::LoggerImpl::default_log_level_ = defaultLogLevel;

  impl::LoggerImpl::file_path_ = LOG_PATH_PRTFIX + filePath;
  std::cout << "log_path: " << impl::LoggerImpl::file_path_ << std::endl;
  // get file_path's dir
  size_t pose = 1;
  while(true) {
    auto tmp_pose = impl::LoggerImpl::file_path_.find("/", pose);
    if(tmp_pose == impl::LoggerImpl::file_path_.npos) break;

    std::string dir = impl::LoggerImpl::file_path_.substr(0, tmp_pose);
    // create it if it not exsit
    if(access(dir.c_str(), F_OK) != 0) {
      if(mkdir(dir.c_str(), 0777) != 0) {
        std::cout << "create log dir failed :" << strerror(errno) << std::endl;
      }
    }
    pose = tmp_pose + 1;
  }

  impl::LoggerImpl::mode_ = mode;
  impl::LoggerImpl::ApplicationId_ = applicationId;
  impl::LoggerImpl::ApplicationDescription_ = applicationDescription;
  impl::LoggerImpl::register_handler();
  auto at_exit = [](){
    Logger::Deinitialize();
  };
  std::atexit(at_exit);

  return impl::LoggerImpl::init();
}

bool Logger::Deinitialize() noexcept {

  if (!impl::LoggerImpl::is_running_.load(std::memory_order_release)) {
    return false;
  }

  impl::LoggerImpl::deinitialized();
  while(true){
    int read_conf = 0;
    if(loggers_ref.compare_exchange_weak(read_conf, -1, std::memory_order_release)){
      break;
    }
  }

  loggers.clear();
  loggers_ref.store(0, std::memory_order_consume);

  return true;
}

Logger& Logger::CreateLogger(std::string const contextId,
                             std::string const contextDescription,
                             LogLevel const severityLevel) noexcept {
  std::shared_ptr<hanhai::log::Logger> logger = nullptr;

  int read_conf = loggers_ref.load(std::memory_order_release);
  if(read_conf == -1) read_conf = 0;

  while(!loggers_ref.compare_exchange_weak(read_conf, read_conf + 1, std::memory_order_consume)){
    if(read_conf == -1) read_conf = 0;
  }

  auto result = loggers.find(contextId);
  while(!loggers_ref.compare_exchange_weak(read_conf, read_conf - 1, std::memory_order_consume));

  
  if(result == loggers.end()){
    while(true){
      int read_conf = 0;
      if(loggers_ref.compare_exchange_weak(read_conf, -1, std::memory_order_release)){
        break;
      }
    }
    std::shared_ptr<Logger> tmp_logger(new Logger());
    tmp_logger->context_id_ = contextId;
    tmp_logger->context_description_ = contextDescription;
    tmp_logger->impl_ =
    new impl::LoggerImpl(contextId, contextDescription, severityLevel);

    loggers.insert(std::make_pair(contextId, tmp_logger));
    loggers_ref.store(0, std::memory_order_consume);
    logger = tmp_logger;
  } else {
    logger = result->second;
    logger->severity_level = severityLevel;
  }

  return *logger;
}

void RegisterCustom(LogCustomer* consumer) noexcept {
  impl::LoggerImpl::RegisterCustom(consumer);
}

bool Logger::IsEnabled(LogLevel severityLevel) const noexcept {
  return severityLevel <= impl_->default_log_level_ && severityLevel <= impl_->severity_level_;
}

#define CREATE_LOGSTREAM(level)                           \
  LogStream stream(level);                                \
  stream.log_impl_ = impl_;                               \
  if (level <= default_log_level_) stream.enabled = true; \
  return std::move(stream);

LogStream Logger::LogError() noexcept { CREATE_LOGSTREAM(LogLevel::kError); }
LogStream Logger::LogWarn() noexcept { CREATE_LOGSTREAM(LogLevel::kWarn); }
LogStream Logger::LogInfo() noexcept { CREATE_LOGSTREAM(LogLevel::kInfo); }
LogStream Logger::LogDebug() noexcept { CREATE_LOGSTREAM(LogLevel::kDebug); }
LogStream Logger::WithLevel(LogLevel const severityLevel) noexcept {
  CREATE_LOGSTREAM(severityLevel);
}

std::string& Logger::ApplicationId_ = impl::LoggerImpl::ApplicationId_;
std::string& Logger::ApplicationDescription_ =
    impl::LoggerImpl::ApplicationDescription_;
LogLevel Logger::default_log_level_ {};

std::map< const std::string, const std::shared_ptr<Logger>> Logger::loggers {};
std::atomic_int Logger::loggers_ref {0};

LogMode LogMode::kRemote = LogMode(REMOTE_MODE);
LogMode LogMode::kFile = LogMode(FILE_MODE);
LogMode LogMode::kConsole = LogMode(CONSOLE_MODE);
LogMode LogMode::kCustom = LogMode(CUSTOM_MODE);
}  // namespace log
}  // namespace hanhai