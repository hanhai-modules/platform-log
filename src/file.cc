/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "hanhai-platform-log/file.h"

namespace hanhai {
namespace log {
using google::protobuf::TextFormat;
using google::protobuf::io::FileInputStream;
using google::protobuf::io::ZeroCopyInputStream;
using Message = google::protobuf::Message;

bool ParseProtoConfig(std::string path, Message & config) {
  
  bool result = false;
  int file_descriptor = open(path.c_str(), O_RDONLY);
  if (file_descriptor < 0) {
    return result;
  }
  ZeroCopyInputStream* input = new FileInputStream(file_descriptor);

  try {
    bool success = TextFormat::Parse(input, &config);
    if (!success) {
      std::cout << "Error parsing configuration: " << path
                << std::endl;
      goto parse_over;
    }

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    goto parse_over;
  }

  result = true;

  parse_over:
    delete input;
    close(file_descriptor);
    return result;
}

std::string ParseDir(std::string &path)
{
    int pose = 1;
    while (true)
    {
        auto tmp_pose = path.find("/", pose);
        if (tmp_pose == path.npos)
            break;
        pose = tmp_pose + 1;
    }
    return path.substr(0, pose - 1);
}

}
}