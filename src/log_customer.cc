/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/log_customer.h"

namespace hanhai {
namespace log {

LogCustomer::LogCustomer(bool async, bool through_mode)
#if USE_BUILTIN_LOG_SYS
  : async_(async), through_mode_(through_mode) 
#endif
{
  is_running_ = true;
#if USE_BUILTIN_LOG_SYS
  if(async_ && !through_mode_) {
    sem_init(&sem_for_new_message_, 0, 0);
    proccessor = std::move(std::thread([this]() { proccess(); }));
  }
#endif // USE_BUILTIN_LOG_SYS
}
void LogCustomer::Deinitialize() {
  if (!is_running_) return;
  is_running_ = false;
#if USE_BUILTIN_LOG_SYS
  if(async_ && !through_mode_){
    proccessor.join();
    while (!messages_.empty()) {
      Consume(messages_.front());
      messages_.pop_front();
    }
    flush();
  }
#endif
}

#if USE_BUILTIN_LOG_SYS
void LogCustomer::ConsumeMsg(PlatformLogMessagePtr const& message) {
  if (message->empty()) return;
  if(!async_ || through_mode_){
    Consume(message);
    return;
  }
  
  {
    std::lock_guard<std::mutex> lock(messages_mutex_);
    messages_.push_back(message);
  }

  sem_post(&sem_for_new_message_);
}
void LogCustomer::proccess() {
  timeval now;
  timespec flush_time;
  while (is_running_ || !messages_.empty()) {
    if (messages_.empty()) {
      gettimeofday(&now, NULL);
      flush_time.tv_sec = now.tv_sec;
      if (now.tv_usec < (1e6 - CUSTOMER_FLUSH_INTER * 1000)) {
        flush_time.tv_sec = now.tv_sec;
        flush_time.tv_nsec = (now.tv_usec + CUSTOMER_FLUSH_INTER * 1000) * 1000;
      } else {
        flush_time.tv_sec = now.tv_sec + 1;
        flush_time.tv_nsec =
            (now.tv_usec + (CUSTOMER_FLUSH_INTER * 1000 - 1e6)) * 1000;
      }
      int result = sem_timedwait(&sem_for_new_message_, &flush_time);
      if (result != 0) {
        flush();
        continue;
      }
    }
    PlatformLogMessagePtr msg;
    while (!messages_.empty()) {
      msg = messages_.front();
      {
        std::lock_guard<std::mutex> lock(messages_mutex_);
        messages_.pop_front();
      }
      Consume(msg);
    }
  }
}

#endif // USE_BUILTIN_LOG_SYS
}  // namespace log
}  // namespace hanhai
