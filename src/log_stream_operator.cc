/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include <memory.h>
#include <stdlib.h>
#include <unistd.h>

#include "impl/log_impl.h"
#include "hanhai-platform-log/log_message.h"
#include "hanhai-platform-log/log_stream.h"

#define LOG_STREAM_OPERATOR(type, ADS_TYPE)                                  \
  LogStream& LogStream::operator<<(type value) noexcept {                    \
    if (!enabled) return *this;                                              \
    auto var = new PlatformLogVariable();                                         \
    var->data_type = ADSDataType::ADS_TYPE;                                  \
    var->dlt_size = 0;                                                       \
    var->unit_size = 0;                                                      \
    var->data_size = sizeof(type);                                           \
    var->buff.store(new uint8_t[var->data_size], std::memory_order_consume); \
    *(type*)(var->buff.load(std::memory_order_release)) = value;             \
    this->message_->push(var);                                               \
    return *this;                                                            \
  }

namespace hanhai {
namespace log {

LOG_STREAM_OPERATOR(bool, ADS_BOOL)
LOG_STREAM_OPERATOR(uint8_t, ADS_UINT8)
LOG_STREAM_OPERATOR(uint16_t, ADS_UINT16)
LOG_STREAM_OPERATOR(uint32_t, ADS_UINT32)
LOG_STREAM_OPERATOR(uint64_t, ADS_UINT64)
LOG_STREAM_OPERATOR(int8_t, ADS_INT8)
LOG_STREAM_OPERATOR(int16_t, ADS_INT16)
LOG_STREAM_OPERATOR(int32_t, ADS_INT32)
LOG_STREAM_OPERATOR(int64_t, ADS_INT64)
LOG_STREAM_OPERATOR(float, ADS_FLOAT)
LOG_STREAM_OPERATOR(double, ADS_DOUBLE)
LOG_STREAM_OPERATOR(Format, ADS_FORMAT)

LogStream& LogStream::operator<<(Fmt value) noexcept {                    
  return (*this) << Format(value);                                                          
}
LogStream& LogStream::operator<<(std::string value) noexcept {
  if(!enabled) return *this;

  auto var = new PlatformLogVariable();
  var->data_type = ADSDataType::ADS_STRING;
  var->dlt_size = 0;
  var->unit_size = 0;
  var->data_size = value.size();
  var->buff.store(new uint8_t[var->data_size], std::memory_order_consume);
  memcpy(var->buff.load(std::memory_order_release), value.c_str(),
         var->data_size);
  this->message_->push(var);
  return *this;
}
LogStream& LogStream::operator<<(const char* value) noexcept {
  if(!enabled) return *this;

  auto var = new PlatformLogVariable();
  var->data_type = ADSDataType::ADS_STRING;
  var->dlt_size = 0;
  var->unit_size = 0;
  var->data_size = strlen(value);
  var->buff.store(new uint8_t[var->data_size], std::memory_order_consume);
  memcpy(var->buff.load(std::memory_order_release), value, var->data_size);
  message_->push(var);
  return *this;
}
LogStream& LogStream::operator<<(char value) noexcept {
  if(!enabled) return *this;

  auto var = new PlatformLogVariable();
  var->data_type = ADSDataType::ADS_STRING;
  var->dlt_size = 0;
  var->unit_size = 0;
  var->data_size = 1;
  var->buff.store(new uint8_t[var->data_size], std::memory_order_consume);
  *(char*)(var->buff.load(std::memory_order_release)) = value;
  this->message_->push(var);
  return *this;
}
LogStream& LogStream::operator<<(const void* const value) noexcept {
  if(!enabled) return *this;

  return *this;
}

std::ostream& operator<<(std::ostream& out, LogStream& stream) noexcept {
  if(!stream.enabled) return out;

  out << stream.message_;
  return out;
}

}  // namespace log
}  // namespace hanhai