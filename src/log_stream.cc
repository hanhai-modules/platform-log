/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "impl/log_impl.h"
#include "hanhai-platform-log/log_stream.h"

namespace hanhai {
namespace log {
LogStream::LogStream(LogLevel const severityLevel) {
  message_ = std::make_shared<PlatformLogMessage>();
  message_->log_level = severityLevel;
}

LogStream::~LogStream(){
    inner_flush_ = true;
    flush();
}

void LogStream::flush() noexcept {
  if(!enabled) return;
  
  if(log_impl_ == nullptr) return;

  if (message_ != nullptr) {
    log_impl_->push_message(message_);
    message_ = nullptr;
  }
  auto severity_level_ = message_->log_level;

  if (!inner_flush_) {
    message_ = std::make_shared<PlatformLogMessage>();
    message_->log_level = severity_level_;
  }
}

LogStream& LogStream::operator=(LogStream && other){
  message_ = other.message_;
  log_impl_ = other.log_impl_;
  inner_flush_ = other.inner_flush_;

  other.message_ = nullptr;
  other.log_impl_ = nullptr;
  other.enabled = enabled;
  return *this;
}

LogStream::LogStream(LogStream && other){
  message_ = other.message_;
  log_impl_ = other.log_impl_;
  inner_flush_ = other.inner_flush_;
  enabled = other.enabled;
  
  other.message_ = nullptr;
  other.log_impl_ = nullptr;
}

LogStream& LogStream::WithLocation (std::string file, uint16_t line) noexcept{
  return WithLocation(file, line, "");
}
LogStream& LogStream::WithLocation (std::string file, uint16_t line, std::string func) noexcept{
  message_->location.within_location = true;
  message_->location.file = file;
  message_->location.line = line;
  message_->location.function = func;
  return *this;
}

}  // namespace log
}  // namespace hanhai