/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "thirdparty_logger_loader.h"
#include "thirdparty_logger_loader_utity.h"

#include <dlfcn.h>

#include <map>
#include <utility>
#include <tuple>
#include <list>

#define CHECK_ERROR(err) (err ? err : "unknow error")

namespace hanhai {
namespace log {

static std::map<std::string, std::pair<void*, std::list<ThirdpartyLogger*>>> third_party_loggers_;

std::string find_lib_by_logger(ThirdpartyLogger* logger) {
    if(!logger) return "";

    for(auto& third_party_logger : third_party_loggers_) {
        for(auto cache_logger : third_party_logger.second.second) {
            if(cache_logger == logger) {
                return third_party_logger.first;
            }
        }
    }

    return "";
}

void close_thirdparty_logger(std::string & path, ThirdpartyLogger* logger) {
    if(third_party_loggers_.find(path) == third_party_loggers_.end()) {
        return;
    }

    if(logger) {
        auto& loggers = third_party_loggers_[path].second;
        for(auto it = loggers.cbegin(); it != loggers.end(); ++it){
            if(*it == logger) {
                loggers.erase(it);
                break;
            }
        }
    }

    if(third_party_loggers_[path].second.empty()){
        if(dlclose(third_party_loggers_[path].first) != 0) {
            const char * err = dlerror();
            std::cerr << "[close_thirdparty_logger error]: can't close lib '" << 
                    path << "', cause: " << CHECK_ERROR(err) << std::endl;
        }

        if(0 == third_party_loggers_.erase(path)) {
            std::cerr << "[close_thirdparty_logger error]: can't remove lib : " << path << std::endl;
        }
    }
}

ThirdpartyLogger * load_thirdparty_logger(std::string & path){
    if(third_party_loggers_.find(path) == third_party_loggers_.end()) {
        void * handle = dlopen(path.c_str(), RTLD_LAZY);
        if(!handle){
            const char * err = dlerror();
            std::cerr << "load thirdparty logger failed: " << CHECK_ERROR(err) << std::endl;
            return nullptr;
        }
        third_party_loggers_[path] = std::pair<void*, std::list<ThirdpartyLogger*>>(handle, std::list<ThirdpartyLogger*>());
    }

    void* handle = third_party_loggers_[path].first;

    ThirdpartyLoggerProductor productor = (ThirdpartyLoggerProductor) dlsym(handle, "CreateThirdpartyLogger");
    if(!productor){
        const char * err = dlerror();
        std::cerr << "load symbol `CreateThirdpartyLogger` failed: " << CHECK_ERROR(err) << std::endl;
        close_thirdparty_logger(path, nullptr);
        return nullptr;
    }

    ThirdpartyLogger* ret = productor();

    if(ret){
        third_party_loggers_[path].second.push_back(ret);
    }

    return ret;
}
} // namespace log
}  // namespace hanhai