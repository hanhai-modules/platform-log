/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef __ADS_LOGGER_THIRDPARTY_LOGGER_LOADER_H__
#define __ADS_LOGGER_THIRDPARTY_LOGGER_LOADER_H__

#include "hanhai-platform-log/thirdparty_logger.h"

namespace hanhai {
namespace log {
ThirdpartyLogger * load_thirdparty_logger(std::string & path);
} // namespace log
}  // namespace hanhai

#endif // __ADS_LOGGER_THIRDPARTY_LOGGER_LOADER_H__