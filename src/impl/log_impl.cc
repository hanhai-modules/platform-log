/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "log_impl.h"

#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

#include <algorithm>
#include <chrono>
#include <iostream>

#include "hanhai-platform-log/base_utility.h"
#include "hanhai-platform-log/global_config.h"
#include "hanhai-platform-log/log_type.h"
#include "hanhai-platform-log/thirdparty_logger.h"
#include "thirdparty_logger_loader.h"
#include "thirdparty_logger_loader_utity.h"
#include <hanhai-platform-log/iostream_wrapper.h>

namespace hanhai {
namespace log {
namespace impl {

LoggerImpl::LoggerImpl(std::string context_id, std::string context_description,
                       LogLevel severity_level) {
#if USE_BUILTIN_LOG_SYS
  std::lock_guard<std::mutex> lock(mutex_for_productor_);
#endif
  context_id_ = context_id;
  context_description_ = context_description;
  severity_level_ = severity_level;
#if USE_BUILTIN_LOG_SYS
  auto thread_id = std::this_thread::get_id();
  auto opration = [&]() -> bool {
    auto queues = front_message_queues_.load(std::memory_order_release);
    RETURN_IF_NULL(queues, false);
    return queues->find(thread_id) != queues->end();
  };

  if (oprate_front_message_queue(opration)) return;

  auto msg = std::make_shared<PlatformLogMessage>();
  enter_thread_id_pre_queue(msg);
  if (sem_for_new_message_) sem_post(sem_for_new_message_);
#endif
}

bool LoggerImpl::init() noexcept {
  if (is_running_.load()) {
    return true;
  }
#if USE_BUILTIN_LOG_SYS
  for (int index = 0; index < PLATFORM_LOG_THREAD_NUM_MAX; index++) {
    pre_msg_queue_for_new_thread_[index].rw_lock.store(
        0, std::memory_order_seq_cst);
    pre_msg_queue_for_new_thread_[index].id = std::thread::id(0);
  }
#endif

  pid_.store(getpid());
  is_running_.store(true);
#if USE_BUILTIN_LOG_SYS
  random_engine_.seed(time(0));
  front_message_queues_.store(new impl::MessageMap(),
                              std::memory_order_seq_cst);
  back_message_queues_ = new impl::MessageMap();

  if (nullptr == sem_for_new_message_) {
    sem_for_new_message_ = new sem_t;
    sem_init(sem_for_new_message_, 0, 0);
  }
#endif // USE_BUILTIN_LOG_SYS
  GlobalConfiguration::init();

  if((mode_ & LogMode::kConsole) == LogMode::kConsole){
    std::string lib_path = GlobalConfiguration::thirdparty_root_directory+"/libconsole_logger.so";
    auto console_logger = load_thirdparty_logger(lib_path);
    console_logger->Initialize(ApplicationId_.c_str(), ApplicationDescription_.c_str(),
                          default_log_level_, mode_, file_path_.c_str());
    thirdparty_loggers_.push_back(console_logger);
    RegisterCustom(console_logger->customer);
  }
  if((mode_ & LogMode::kFile) == LogMode::kFile){
    auto platform_log_impl = getenv("PLATFORM_LOG_IMPL");
    
    if(nullptr != platform_log_impl){
      auto config_path = std::string(platform_log_impl);
      ThirdpartyLogger* logger = load_thirdparty_logger(config_path);
      if (logger) {
        logger->Initialize(ApplicationId_.c_str(), ApplicationDescription_.c_str(),
                          default_log_level_, mode_, file_path_.c_str());
        thirdparty_loggers_.push_back(logger);
        RegisterCustom(logger->customer);
      }
      return true;
    }
    auto confs = GlobalConfiguration::get_configurations();
    for (auto& conf : confs) {
      ThirdpartyLogger* logger = load_thirdparty_logger(conf.path);
      if (logger) {
        logger->Initialize(ApplicationId_.c_str(), ApplicationDescription_.c_str(),
                          default_log_level_, mode_, file_path_.c_str());
        thirdparty_loggers_.push_back(logger);
        RegisterCustom(logger->customer);
      }
    }
  }
#if USE_BUILTIN_LOG_SYS
  message_prossesor_thread_ =
      new std::thread(impl::LoggerImpl::message_prossesor);
#endif
  return true;
}

void LoggerImpl::exit_handler() noexcept {
  bool log_status = true;
  while (!is_running_.compare_exchange_weak(log_status, false,
                                            std::memory_order_release)) {
    if (!log_status) return;
    usleep(PLATFORM_LOG_PROSSESOR_FLUSH_TIME);
  }
#if USE_BUILTIN_LOG_SYS
  if (message_prossesor_thread_) {
    if (sem_for_new_message_) sem_post(sem_for_new_message_);
    message_prossesor_thread_->join();
    delete message_prossesor_thread_;
    message_prossesor_thread_ = nullptr;
  }
#endif
}



void LoggerImpl::deinitialized() noexcept {
  if (!is_running_.load(std::memory_order_seq_cst)) return;

  if (pid_.load(std::memory_order_release) != getpid()) {
#if USE_BUILTIN_LOG_SYS
  
    front_message_queues_ref_count_.store(0, std::memory_order_seq_cst);
    // is_running_.store(false, std::memory_order_seq_cst);
#endif
  } else {
    exit_handler();
    goto deinitialized_over;
  }

  {  // clear all msg
#if USE_BUILTIN_LOG_SYS
    message_prossesor_thread_ = nullptr;

    clear_back_msg_queue();

    auto before_swap = std::chrono::system_clock::now();
    while (!swap_map()) {
      if (std::chrono::system_clock::now() - before_swap >
          std::chrono::microseconds(PLATFORM_LOG_PROSSESOR_FLUSH_TIME)) {
        break;
      }
    }
    clear_back_msg_queue();
#endif
    // restore atomic variables
    pid_.store(0, std::memory_order_seq_cst);
    is_running_.store(false, std::memory_order_seq_cst);

  }  // clear all msg end

deinitialized_over:
  for (auto logger : thirdparty_loggers_) {
    logger->Deinitialize();
    delete logger;
    std::string lib_path = find_lib_by_logger(logger);
    if(!lib_path.empty()){
      close_thirdparty_logger(lib_path, logger);
    }
  }
#if USE_BUILTIN_LOG_SYS
  for (int index = 0; index < PLATFORM_LOG_THREAD_NUM_MAX; index++) {
    int readable_info = 1;
    while (true) {
      if (readable_info == -2 || readable_info == 2) {
        continue;
      }
      if (pre_msg_queue_for_new_thread_[index].rw_lock.compare_exchange_weak(
              readable_info, -1, std::memory_order_consume)) {
        pre_msg_queue_for_new_thread_[index].id = std::thread::id(0);
        pre_msg_queue_for_new_thread_[index].msg.reset();
        break;
      }
    }
  }
#endif
  thirdparty_loggers_.clear();
  customers_.clear();
#if USE_BUILTIN_LOG_SYS
  delete front_message_queues_.load();
  delete back_message_queues_;
  front_message_queues_.store(nullptr, std::memory_order_seq_cst);
  back_message_queues_ = nullptr;
  sem_destroy(sem_for_new_message_);
  delete sem_for_new_message_;
  sem_for_new_message_ = nullptr;
#endif
}

void LoggerImpl::restart_logger() noexcept {
  deinitialized();
  init();
}

void LoggerImpl::register_handler() noexcept {
  pthread_atfork(befor_fork_callback, after_fork_parent_proccess_callback,
                 after_fork_child_proccess_callback);
}

void LoggerImpl::RegisterCustom(LogCustomer* consumer) noexcept {
  if (consumer == nullptr) {
    return;
  }
#if USE_BUILTIN_LOG_SYS
  // std::lock_guard<std::mutex> lock(mutex_for_custom_);
  if(!consumer->through_mode_){
    has_no_through_customer_ = true;
  }
#endif
  customers_.push_back(consumer);
}

void LoggerImpl::befor_fork_callback() noexcept {
#if USE_BUILTIN_LOG_SYS
  mutex_for_productor_.lock();
  mutex_for_custom_.lock();
#endif
}
void LoggerImpl::after_fork_child_proccess_callback() noexcept {
  try {
    ApplicationId_.append("-" + std::to_string(getpid()));
    ApplicationDescription_.append(
        ". For proccess : " + std::to_string(getpid()) + ".");
  #if USE_BUILTIN_LOG_SYS
    for (int index = 0; index < PLATFORM_LOG_THREAD_NUM_MAX; index++) {
      pre_msg_queue_for_new_thread_[index].rw_lock.store(
          1, std::memory_order_seq_cst);
    }
  #endif

    // restart_logger();
    deinitialized();


  #if USE_BUILTIN_LOG_SYS
    mutex_for_custom_.unlock();
    mutex_for_productor_.unlock();
  #endif
  } catch(std::exception& e){
    NOEXCEPT_CERR(<< "after_fork_child_proccess_callback bad: " << e.what() << std::endl);
  }
}

void LoggerImpl::after_fork_parent_proccess_callback() noexcept {
#if USE_BUILTIN_LOG_SYS
  mutex_for_custom_.unlock();
  mutex_for_productor_.unlock();
#endif
}

void LoggerImpl::push_message(PlatformLogMessagePtr& message) {
  if (ITS_UNLIKELY(message == nullptr)) {
    return;
  }

  {
    // std::lock_guard<std::mutex> lock_productor(mutex_for_productor_);

    message->context_id = context_id_;

    for (auto customer : customers_) {
#if USE_BUILTIN_LOG_SYS
      if (!customer->through_mode_) {
        continue;
      };
#endif
      customer->Consume(message);
    }

    if(!has_no_through_customer_) return;
#if USE_BUILTIN_LOG_SYS
    if (message->log_level > severity_level_ ||
        message->log_level > default_log_level_) {
      return;
    }

    if (ITS_UNLIKELY(!is_running_.load(std::memory_order_release))) {
      return;
    }

    auto thread_id = std::this_thread::get_id();
    auto opration = [&]() -> bool {
      auto queues = front_message_queues_.load(std::memory_order_seq_cst);
      hanhai::log::impl::MessageMap::iterator queue(nullptr);
      RETURN_IF_NULL(queues, false);
      queue = queues->find(thread_id);
      if (queue == queues->end()) {
        return false;
      }
      queue->second.push_back(message);
      return true;
    };

    if (!oprate_front_message_queue(opration)) {
      if (!enter_thread_id_pre_queue(message)) {
        // std::cout << "push message failed" << std::endl;
      }
    }
    // cond_for_new_message_.notify_all();
    // if(sem_for_new_message_)
    //   sem_post(sem_for_new_message_);
    return;
#endif
  }
}

std::atomic_int LoggerImpl::pid_ = {0};
std::atomic_bool LoggerImpl::is_running_ = {false};
std::string LoggerImpl::ApplicationId_ = "";
std::string LoggerImpl::ApplicationDescription_ = "";
std::string LoggerImpl::file_path_ = "";
std::vector<LogCustomer*> LoggerImpl::customers_ = {};
std::vector<ThirdpartyLogger*> LoggerImpl::thirdparty_loggers_ = {};
LogMode LoggerImpl::mode_{0x00};
LogLevel LoggerImpl::default_log_level_{};

#if USE_BUILTIN_LOG_SYS
void LoggerImpl::clear_back_msg_queue() noexcept {
  if (back_message_queues_ == nullptr) return;

  back_message_queues_->clear();

  delete back_message_queues_;
  back_message_queues_ = nullptr;
}

bool LoggerImpl::swap_map() noexcept {
  if (new_message_ready_) {
    for (auto queue = back_message_queues_->begin();
         queue != back_message_queues_->end(); queue++) {
      queue->second.clear();
    }

    auto tmp_queue = front_message_queues_.load(std::memory_order_release);
    int before_ref = 0;

    if (front_message_queues_ref_count_.compare_exchange_weak(
            before_ref, -1, std::memory_order_consume)) {
      front_message_queues_.store(back_message_queues_,
                                  std::memory_order_consume);
      back_message_queues_ = tmp_queue;
      new_message_ready_ = false;
      front_message_queues_ref_count_.store(0, std::memory_order_consume);

      tmp_queue = front_message_queues_.load(std::memory_order_release);
      for (auto queue = tmp_queue->begin(); queue != tmp_queue->end();
           queue++) {
        if (back_message_queues_->find(queue->first) ==
            back_message_queues_->end()) {
          back_message_queues_->insert(
              std::make_pair(queue->first, std::list<PlatformLogMessagePtr>()));
        }
      }

      return true;
    }
  }
  return false;
}
bool LoggerImpl::enter_thread_id_pre_queue(PlatformLogMessagePtr& msg) noexcept {
  auto before_block = std::chrono::system_clock::now();
  auto thread_id = std::this_thread::get_id();

  // Start the search from a random pose
  std::uniform_int_distribution<int> random_prodctor(
      0, PLATFORM_LOG_THREAD_NUM_MAX - 1);
  int index = random_prodctor(random_engine_);

  while (is_running_.load(std::memory_order_release)) {
    int available_info = 0;
    if (pre_msg_queue_for_new_thread_[index].rw_lock.compare_exchange_weak(
            available_info, 2, std::memory_order_seq_cst)) {
      // Found a available position
      pre_msg_queue_for_new_thread_[index].id = thread_id;
      pre_msg_queue_for_new_thread_[index].msg = msg;

      pre_msg_queue_for_new_thread_[index].rw_lock.store(
          1, std::memory_order_seq_cst);

      if (sem_for_new_message_) sem_post(sem_for_new_message_);
      return true;
    }

    index++;
    if (index >= PLATFORM_LOG_THREAD_NUM_MAX) {
      index = index % PLATFORM_LOG_THREAD_NUM_MAX;
    }

    if (std::chrono::system_clock::now() - before_block >
        std::chrono::microseconds(PLATFORM_LOG_MAX_BLOCK_TIME)) {
      break;
    }
  }

  return false;
}

void LoggerImpl::message_prossesor() noexcept {
  bool no_more_messages = false;
  std::chrono::_V2::system_clock::time_point last_swap_map_time{};
  bool has_new_thread_join = false;
  // std::unique_lock<std::mutex> new_message_lock(mutex_for_new_message_);
  timespec flush_time;
  timeval now;
  // int sem_value = 0;
  gettimeofday(&now, NULL);
  flush_time.tv_sec = flush_time.tv_sec + 1;

  while (!no_more_messages || is_running_.load(std::memory_order_release)) {
    // std::lock_guard<std::mutex> lock(mutex_for_custom_);
    // usleep(PLATFORM_LOG_PROSSESOR_FLUSH_TIME);
    // cond_for_new_message_.wait_until(new_message_lock,
    // std::chrono::system_clock::now() +
    //                                                       std::chrono::microseconds(PLATFORM_LOG_PROSSESOR_FLUSH_TIME*100));
    if (sem_for_new_message_) {
      gettimeofday(&now, NULL);
      flush_time.tv_sec = now.tv_sec;
      if (now.tv_usec < (1e6 - PLATFORM_LOG_PROSSESOR_FLUSH_INTERVAL_TIME * 1000)) {
        flush_time.tv_sec = now.tv_sec;
        flush_time.tv_nsec =
            (now.tv_usec + PLATFORM_LOG_PROSSESOR_FLUSH_INTERVAL_TIME * 1000) * 1000;
      } else {
        flush_time.tv_sec = now.tv_sec + 1;
        flush_time.tv_nsec =
            (now.tv_usec +
             (PLATFORM_LOG_PROSSESOR_FLUSH_INTERVAL_TIME * 1000 - 1e6)) *
            1000;
      }
      int result = sem_timedwait(sem_for_new_message_, &flush_time);
      (void)result;
    }

    for (int index = 0; index < PLATFORM_LOG_THREAD_NUM_MAX; index++) {
      if (1 != pre_msg_queue_for_new_thread_[index].rw_lock.load(
                   std::memory_order_release)) {
        continue;
      }

      std::thread::id tmp_thread_id = pre_msg_queue_for_new_thread_[index].id;
      if (back_message_queues_->find(tmp_thread_id) ==
          back_message_queues_->end()) {
        has_new_thread_join = true;
        back_message_queues_->insert(
            std::make_pair(tmp_thread_id, std::list<PlatformLogMessagePtr>()));
      } else {
        has_new_thread_join = false;
      }

      auto queue = back_message_queues_->find(tmp_thread_id);
      auto msg_it = queue->second.begin();
      while (true) {
        bool insert_now = false;
        if(msg_it == queue->second.end() || (*msg_it)->seq_ > pre_msg_queue_for_new_thread_[index].msg->seq_){
          insert_now = true;
        }

        if(insert_now){
          queue->second.insert(msg_it, pre_msg_queue_for_new_thread_[index].msg);
           pre_msg_queue_for_new_thread_[index].rw_lock.store(
          0, std::memory_order_consume);
          break;
        }

        if(msg_it == queue->second.end()){
          break;
        }
        msg_it++;
      }
    }

    while (true){
      auto id_queue = sort_messages();
      if (id_queue.empty()) {
        break;
      }
      auto id_it = id_queue.begin();
      for (int i = 0; i < id_queue.size(); i++) {
        auto id_pre = id_it;
        id_it++;
        while(!back_message_queues_->find(*id_pre)->second.empty()){
          auto msg = back_message_queues_->find(*id_pre)->second.front();
          if(id_it != id_queue.end()){
            if(msg->seq_ > back_message_queues_->find(*id_it)->second.begin()->get()->seq_){
              break;
            }
          }
          for (auto customer : customers_) {
            if(customer->through_mode_) continue;
            customer->ConsumeMsg(msg);
          }
          back_message_queues_->find(*id_pre)->second.pop_front();
          if(id_it == id_queue.end()) break;
        }
      }
    }

    for (int index = 0; index < PLATFORM_LOG_THREAD_NUM_MAX; index++) {
      if (pre_msg_queue_for_new_thread_[index].rw_lock.load(
              std::memory_order_release) == 1) {
        no_more_messages = false;
      }
    }

    no_more_messages = no_more_messages || !new_message_ready_;

    if ((std::chrono::system_clock::now() - last_swap_map_time) >=
            std::chrono::microseconds(PLATFORM_LOG_PROSSESOR_SWAP_INTERVAL) ||
        has_new_thread_join) {
      // block for flush time
      auto before_swap = std::chrono::system_clock::now();
      last_swap_map_time = before_swap;
      while (!swap_map() && is_running_.load(std::memory_order_release)) {
        if (std::chrono::system_clock::now() - before_swap >
                std::chrono::microseconds(PLATFORM_LOG_PROSSESOR_FLUSH_TIME) &&
            !has_new_thread_join) {
          break;
        }
      }
    }
    // mutex_for_custom_.unlock();
  }
}

bool LoggerImpl::oprate_front_message_queue(
    std::function<bool()> opration) noexcept {
  if (!is_running_.load(std::memory_order_release)) {
    return false;
  }
  int ref_count =
      front_message_queues_ref_count_.load(std::memory_order_release);

  bool result = false;
  if (-1 == ref_count) {
    return false;
  }
  // std::lock_guard<std::mutex> lock(mutex_for_swap_map_);

  while (!front_message_queues_ref_count_.compare_exchange_weak(
      ref_count, ref_count + 1, std::memory_order_acq_rel)) {
    if (-1 == ref_count || is_running_.load(std::memory_order_release)) {
      return false;
    }
  }

  if (opration != nullptr) {
    result = opration();
  }

  front_message_queues_ref_count_.fetch_sub(1, std::memory_order_consume);
  if (!new_message_ready_) new_message_ready_ = true;
  return result;
}

std::list<std::thread::id> LoggerImpl::sort_messages() noexcept {
  std::list<std::thread::id> result;
  if(back_message_queues_ == nullptr) return result;

  // insertion sort with thread id
  bool has_new_msg = false;
  for (auto it = back_message_queues_->begin();
       it != back_message_queues_->end(); it++) {
    auto& queue = it->second;
    if(queue.empty()) continue;

    if(result.empty()){
      result.push_back(it->first);
      has_new_msg = true;
      continue;
    }

    has_new_msg = true;
    for (auto it_ = result.begin(); it_ != result.end(); it_++) {
      if (queue.begin()->get()->seq_ < back_message_queues_->find(*it_)->second.begin()->get()->seq_){
        result.insert(it_, it->first);
        break;
      }
    }
  }

  if(!has_new_msg) result.clear();
  return std::move(result);
}

std::thread* LoggerImpl::message_prossesor_thread_ = nullptr;
std::atomic<MessageMap*> LoggerImpl::front_message_queues_ = {nullptr};
std::atomic_int LoggerImpl::front_message_queues_ref_count_ = {0};
MessageMap* LoggerImpl::back_message_queues_ = nullptr;

LoggerImpl::LogMessageWithRWLock
    LoggerImpl::pre_msg_queue_for_new_thread_[PLATFORM_LOG_THREAD_NUM_MAX] = {
        {{}, std::thread::id(0), {0}}};

std::default_random_engine LoggerImpl::random_engine_ =
    std::default_random_engine();

std::mutex LoggerImpl::mutex_for_custom_ = {};
std::mutex LoggerImpl::mutex_for_productor_{};
bool LoggerImpl::new_message_ready_ = false;
// std::condition_variable LoggerImpl::cond_for_new_message_ {};
// std::mutex LoggerImpl::mutex_for_new_message_ {};
sem_t* LoggerImpl::sem_for_new_message_ = nullptr;
bool LoggerImpl::has_no_through_customer_ = false;
#endif // USE_BUILTIN_LOG_SYS

}  // namespace impl
}  // namespace log
}  // namespace hanhai