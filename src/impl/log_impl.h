/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef PLATFORM_LOG_IMPL_H
#define PLATFORM_LOG_IMPL_H

#define PLATFORM_LOG_THREAD_NUM_MAX 256 * 4
#define PLATFORM_LOG_MAX_BLOCK_TIME 200               // us
#define PLATFORM_LOG_PROSSESOR_FLUSH_TIME 1           // us
#define PLATFORM_LOG_PROSSESOR_FLUSH_INTERVAL_TIME 1  // ms
#define PLATFORM_LOG_PROSSESOR_SWAP_INTERVAL 500      // us

#define ITS_LIKELY(condition) __builtin_expect(!!(condition), true)
#define ITS_UNLIKELY(condition) __builtin_expect(!!(condition), false)

#include <semaphore.h>

#include <atomic>
#include <chrono>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <random>
#include <string>
#include <thread>

#include "config.h"

#include "hanhai-platform-log/log_customer.h"
#include "hanhai-platform-log/log_message.h"
#include "hanhai-platform-log/thirdparty_logger.h"

namespace hanhai {
namespace log {
namespace impl {
typedef std::map<std::thread::id, std::list<PlatformLogMessagePtr>> MessageMap;

class LoggerImpl {
 public:
  LoggerImpl(std::string context_id, std::string const context_description,
             LogLevel const severity_level);
  void push_message(PlatformLogMessagePtr& message);

  std::string context_id_;
  std::string context_description_;
  LogLevel severity_level_;

  static bool init() noexcept;
  static void deinitialized() noexcept;
  static void restart_logger() noexcept;
  static void RegisterCustom(LogCustomer* consumer) noexcept;
  static void register_handler() noexcept;

  
  static std::atomic_int pid_;
  static std::atomic_bool is_running_;
  static std::string ApplicationId_;
  static std::string ApplicationDescription_;
  static std::string file_path_;
  static LogLevel default_log_level_;
  static LogMode mode_;

 private:
#if USE_BUILTIN_LOG_SYS
  static void message_prossesor() noexcept;
  static bool enter_thread_id_pre_queue(PlatformLogMessagePtr& msg) noexcept;
  static bool swap_map() noexcept;
  static void clear_back_msg_queue() noexcept;
  static bool oprate_front_message_queue(
      std::function<bool()> opration) noexcept;
  static std::list<std::thread::id> sort_messages() noexcept;
  static std::mutex mutex_for_custom_;
  static std::mutex mutex_for_productor_;

  static bool new_message_ready_;
  // static std::condition_variable cond_for_new_message_;
  // static std::mutex mutex_for_new_message_;
  static sem_t* sem_for_new_message_;

  static std::atomic<MessageMap*> front_message_queues_;
  static std::atomic_int front_message_queues_ref_count_;
  static MessageMap* back_message_queues_;
  static std::thread* message_prossesor_thread_;
  struct {
    PlatformLogMessagePtr msg;
    std::thread::id id;
    std::atomic_int rw_lock;  // 0: Available; 1: Readable; 2: Writing now; -1:
                              // unavailable; -2: blocking
  };
  static LogMessageWithRWLock
      pre_msg_queue_for_new_thread_[PLATFORM_LOG_THREAD_NUM_MAX];
  static std::default_random_engine random_engine_;
#endif
  static void befor_fork_callback() noexcept;
  static void after_fork_child_proccess_callback() noexcept;
  static void after_fork_parent_proccess_callback() noexcept;
  static void exit_handler() noexcept;

  static bool has_no_through_customer_;

  static std::vector<LogCustomer*> customers_;
  static std::vector<ThirdpartyLogger*> thirdparty_loggers_;
};

}  // namespace impl
}  // namespace log
}  // namespace hanhai

#endif  // PLATFORM_LOG_IMPL_H