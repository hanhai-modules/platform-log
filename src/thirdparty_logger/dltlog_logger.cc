/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include <string.h>
#include <fstream>
#include <iostream>
#include <mutex>
#include <sstream>

#include <dlt/dlt.h>
#include <dlt/dlt_common.h>
#include "hanhai-platform-log/thirdparty_logger_impl.h"

#define DEFAULT_FILE_NAME "hanhai-platform-log.log"
#define DLT_STORAGE_CON "/etc/hanhai/conf/platform_log/dlt_logstorage.conf"
#define FILTER_NAME "FILTER"
#define CONTEX_DESCRIPTION "con_text"
#define DEFAULT_CONTEXT "CON"

DLT_DECLARE_CONTEXT(con_exa1);
using namespace hanhai::log;
class DltLogger : public ThirdpartyLogger
{
public:
    ~DltLogger()
    {
        DLT_UNREGISTER_CONTEXT(con_exa1);
        DLT_UNREGISTER_APP();
        Deinitialize();
    }
    class DltLoggerCustomer : public LogCustomer
    {
    public:
        DltLoggerCustomer()
            : LogCustomer(true, false){};
        virtual ~DltLoggerCustomer()
        {
            LogCustomer::Deinitialize();
        }

        virtual void Consume(PlatformLogMessagePtr const& message) override
        {
            const char* file = "";
            uint16_t line = 0;
            if (message->location.within_location) {
                file = message->location.file.c_str();
                line = message->location.line;
                stringbuff << "[" << file << ":" << line << "]:";
            }

            for (auto const& var : *message->variables.load()) {
                const_cast<PlatformLogMessagePtr&>(message)->show_var(stringbuff, *var);
            }
            //int level
            switch (message->log_level) {
            case LogLevel::kVerbose: {
                DLT_LOG(con_exa1, DLT_LOG_VERBOSE, DLT_STRING(stringbuff.str().c_str()));
            } break;

            case LogLevel::kDebug: {
                DLT_LOG(con_exa1, DLT_LOG_DEBUG, DLT_STRING(stringbuff.str().c_str()));
            } break;

            case LogLevel::kInfo: {
                DLT_LOG(con_exa1, DLT_LOG_INFO, DLT_STRING(stringbuff.str().c_str()));
            } break;

            case LogLevel::kWarn: {
                DLT_LOG(con_exa1, DLT_LOG_WARN, DLT_STRING(stringbuff.str().c_str()));
            } break;

            case LogLevel::kError: {
                DLT_LOG(con_exa1, DLT_LOG_ERROR, DLT_STRING(stringbuff.str().c_str()));
            } break;

            case LogLevel::kFatal: {
                DLT_LOG(con_exa1, DLT_LOG_FATAL, DLT_STRING(stringbuff.str().c_str()));
            } break;
            }
            stringbuff.clear();
            stringbuff.str("");
        }
        virtual void flush() {}
        std::stringstream stringbuff;

    } dltlog_customer;

    virtual bool Initialize(const char* applicationId, const char* applicationDescription,
                            LogLevel const defaultLogLevel, LogMode const mode,
                            const char* filePath) noexcept override
    {
        (void)applicationId;
        (void)applicationDescription;
        (void)defaultLogLevel;
        (void)mode;
        (void)filePath;
        if (filePath == nullptr) {
            std::cout << "Please specify log file path. before that, the log system while not work."
                      << std::endl;
            return false;
        }

        is_initialized = true;
        customer = &dltlog_customer;

        std::string tmp_appId = std::string(applicationId);
        auto tmp_pose = tmp_appId.find_last_of("/");
        std::cout << "**********tmp_appId   is: " << tmp_appId << std::endl;
        if (tmp_pose != tmp_appId.npos) {
            tmp_appId.erase(0, tmp_pose + 1);
        }

        // if (tmp_appId.find("./") != -1) {
        //     tmp_appId.erase(0, 2);
        // }
        //去掉增加的log_前缀

        //std::string app_name = tmp_appId.substr(4);
        DLT_REGISTER_APP((const char*)tmp_appId.c_str(), applicationDescription);
        std::string tmp_context;
        if (tmp_appId.size() > 4) {
            tmp_context = tmp_appId.substr(4);
        }
        else {
            tmp_context = DEFAULT_CONTEXT;
        }
        DLT_REGISTER_CONTEXT(con_exa1, tmp_context.c_str(), CONTEX_DESCRIPTION);
        SetConfig(tmp_appId, filePath, tmp_context);

        return true;
    }

    void SetConfig(const std::string& appName, const char* filePath, const std::string& context)
    {
        std::ifstream file_open_stream;
        std::ofstream file_writer_stream;
        if (std::string(DLT_STORAGE_CON).empty())
            filePath = DEFAULT_FILE_NAME;
        file_open_stream.open(DLT_STORAGE_CON);
        std::string line;
        int app_counter_ = 0;
        if (file_open_stream.is_open()) {
            while (getline(file_open_stream, line)) {
                if (line.find(appName) != -1 && line.find("#") == -1) {
                    return;
                }
                if (line.find(FILTER_NAME) != -1 && line.find("#") == -1) {
                    app_counter_++;
                }
            }
        }
        else {
            std::cout << "not oepn file!" << std::endl;
        }
        file_open_stream.close();
        file_writer_stream.open(DLT_STORAGE_CON, std::ios::app);
        if (file_writer_stream.is_open()) {
            file_writer_stream << "[FILTER" << std::to_string(app_counter_) << "]"
                               << "\n";
            file_writer_stream << "LogAppName=" << appName << "\n";
            file_writer_stream << "ContextName=" << context << "\n";
            file_writer_stream << "LogLevel="
                               << "DLT_LOG_WARN"
                               << "\n";
            file_writer_stream << "File=" << filePath << "\n";
            file_writer_stream << "FileSize="
                               << "10000"
                               << "\n";
            file_writer_stream << " NOFiles="
                               << "10"
                               << "\n";
        }

        file_writer_stream.close();
    }


    void Deinitialize() noexcept override
    {
        if (!is_initialized)
            return;
        is_initialized = false;
        customer->Deinitialize();
        customer = nullptr;
    }
};

hanhai::log::ThirdpartyLogger* CreateThirdpartyLogger()
{
    return new DltLogger();
}