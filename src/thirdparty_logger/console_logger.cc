/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/thirdparty_logger_impl.h"
#include "hanhai-platform-log/version/version.h"
#include <iostream>
#include <mutex>
#include <sstream>

#define MAX_BUFF_SIZE 1 // kb

using namespace hanhai::log;

class ConsoleLogger : public ThirdpartyLogger {
 public:
  ~ConsoleLogger() { Deinitialize(); }
  class ConsoleCustomer : public LogCustomer {
   public:
    ConsoleCustomer() : LogCustomer(false, false) {};
    virtual ~ConsoleCustomer() {
      LogCustomer::Deinitialize();
    }
    virtual void Consume(PlatformLogMessagePtr const & message) override{

      std::cout << *message << std::endl;
      // if(stringbuff.str().size() > 1024 * MAX_BUFF_SIZE){
        // std::cout << stringbuff.str();
        // stringbuff.str("");
      // }
    }

    virtual void flush(){
      if(stringbuff.str().empty()) return;

      std::cout << stringbuff.str();
      stringbuff.str("");
    }

    std::stringstream stringbuff;
    
  } console_customer;

  bool Initialize(const char * applicationId,
                  const char * applicationDescription,
                  LogLevel const defaultLogLevel, LogMode const mode,
                  const char * filePath) noexcept override {
    (void) applicationId;
    (void) applicationDescription;
    (void) defaultLogLevel;
    (void) mode;
    (void) filePath;
    
    is_initialized = true;
    customer = &console_customer;
    return true;
  }

  void Deinitialize() noexcept override {
    if(!is_initialized) return;
    is_initialized = false;
    customer->Deinitialize();
    customer = nullptr;
  }
};

hanhai::log::ThirdpartyLogger* CreateThirdpartyLogger() {
  return new ConsoleLogger();
}