/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "glog_file_manager.h"
#include <memory.h>
#include <errno.h>
#include <string.h>

#define REMOVE_FILE(file)                                                                         \
    do                                                                                            \
    {                                                                                             \
        if (remove(file) != 0)                                                                    \
        {                                                                                         \
            std::cerr << "remove file failed(" << errno << "): " << strerror(errno) << std::endl; \
        }                                                                                         \
    } while (0)

namespace hanhai {
namespace log {
bool GetLogFiles(const std::string dir, const std::string file_name, LogFiles &log_files)
{
    struct dirent *dp;
    DIR *dfd = nullptr;
    if ((dfd = opendir(dir.c_str())) == nullptr)
    {
        std::cerr << "GetLogFiles:can not open : " << dir << std::endl;
        return false;
    }

    while ((dp = readdir(dfd)) != nullptr)
    {
        // skip current dir and parent dir
        if ((strcmp(dp->d_name, ".") != 0) && (strcmp(dp->d_name, "..") != 0))
        {
            char name[MAX_NAME_SIZE];
            snprintf(name, MAX_NAME_SIZE, "%s/%s", dir.c_str(), dp->d_name);

            struct stat st_buf;
            if (stat(name, &st_buf) != 0)
            {
                std::cerr << "GetLogFiles:can not access to : " << name << std::endl;
                continue;
            }
            if ((st_buf.st_mode & S_IFMT) != S_IFDIR)
            {
                if (strstr(dp->d_name, file_name.c_str()) == nullptr)
                {
                    continue;
                }
                std::string full_file_name(dp->d_name);
                // get file Num
                int pre_pose = full_file_name.find("[");
                int post_pose = full_file_name.find("]");
                if(pre_pose == std::string::npos || post_pose == std::string::npos) {
                    // std::cerr << "can't get file num" << std::endl;
                    continue;
                }

                uint32_t file_num = atoi(full_file_name.substr(pre_pose+1, post_pose - pre_pose - 1).c_str());

                if(file_num <= 0) {
                    std::cerr << "can't trans file num" << std::endl;
                    continue;
                }

                // get date info from file name
                std::string date_str = full_file_name.substr(post_pose+2, 
                                            full_file_name.find(".") - post_pose - 2);
                const char *format = "%Y%m%d-%H%M%S";
                struct tm * tmp_tm = nullptr;
                tmp_tm = new (struct tm);
                {
                    memset(tmp_tm, '0', sizeof(struct tm));
                    if(!strptime(date_str.c_str(), format, tmp_tm)){
                        std::cerr << "clc file ts failed" << std::endl;
                        goto clear_tm;
                    }

                    tmp_tm->tm_isdst = 0;

                    time_t time_stamp = {};
                    time_stamp = mktime(tmp_tm);

                    if(time_stamp <= 0) {
                        std::cerr << "clc file ts: "<< time_stamp << ", file name: " << dp->d_name << std::endl;
                        goto clear_tm;
                    }
                    if (log_files.find(file_num) != log_files.end())
                    {
                        std::cerr << "file_num: " << file_num << " repeat" << std::endl;
                        goto clear_tm;
                    }

                    LogFileAttr attr;
                    attr.name = dir + "/" + dp->d_name;
                    attr.size = (long)st_buf.st_size;
                    attr.ts = time_stamp;

                    log_files[file_num] = attr;
                }
               clear_tm:
                delete tmp_tm;
                tmp_tm = nullptr;
            }
        }
    }
    closedir(dfd);
    return true;
}

bool DeleteLogfile(GlogFileLimit limit, LogFiles log_files)
{
    time_t now_time;
    time(&now_time);

    long all_size = 0;
    int file_countor = 0;
    bool clock_steady = true;
    time_t last_time = now_time;
    int max_day = limit.log_file_max_day() * 60 * 60 * 24;

    for (auto file = log_files.rbegin(); file != log_files.rend(); file++)
    {
        if(last_time < file->second.ts) {
            clock_steady = false;
        }
        
        if(++file_countor >= limit.max_file_num()){
            std::cout << "max_file_num: " << limit.max_file_num() << ", file_countor : " 
                      << file_countor << ", file: " 
                      <<  file->second.name << " will be remove"  <<std::endl;
            REMOVE_FILE(file->second.name.c_str());
            continue;
        }

        last_time = file->second.ts;
        if (limit.has_log_file_max_day() && (now_time - file->second.ts) > max_day
            && limit.log_file_max_day() != 0 && clock_steady)
        {
            std::cout << "now_time - file->ts: " << now_time - file->second.ts << ", max_day: " << max_day << std::endl;
            std::cout << "now_time: " << now_time << ", file: " << file->second.ts << std::endl;
            std::cout << "log is too old, file \"" << file->second.name << " will be delete" << std::endl;
            REMOVE_FILE(file->second.name.c_str());
            continue;
        }

        all_size += file->second.size;
        if (limit.has_log_file_max_size() && all_size > limit.log_file_max_size()
            && limit.has_log_file_max_size() != 0)
        {
            std::cout << "log size over, file \"" << file->second.name << " will be delete" << std::endl;
            REMOVE_FILE(file->second.name.c_str());
            continue;
        }
    }

    return true;
}

}  // namespace impl
}  // namespace log
