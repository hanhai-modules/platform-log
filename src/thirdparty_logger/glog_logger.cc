/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/thirdparty_logger_impl.h"

#include <iostream>
#include <mutex>
#include <sstream>

#include <glog/logging.h>
#include "glog/raw_logging.h"
#include "glog_file_manager.h"
#include "glog/glog_file_logger.h"
#include "hanhai-platform-log/version/version.h"
#include "hanhai-platform-log/iostream_wrapper.h"

#define MAX_BUFF_SIZE 1 // kb

using namespace hanhai::log;

class GlogLogger : public ThirdpartyLogger {
 public:
  ~GlogLogger() { Deinitialize(); }
  class GlogCustomer : public LogCustomer {
   public:
    GlogCustomer() : LogCustomer(false, false) {};
    virtual ~GlogCustomer() {
      LogCustomer::Deinitialize();
    }
    virtual void Consume(PlatformLogMessagePtr const & message) override{
      if(!is_running_) return;
      
      google::LogSeverity level;
      const char * log_flag = "";
      switch(message->log_level){
        case LogLevel::kDebug : {
          level  = google::INFO;
          log_flag = "DEBUG";
          break;
        }

        case LogLevel::kInfo : {
          level  = google::INFO;
          log_flag = "INFO";
          break;
        }

        case LogLevel::kWarn : {
          level  = google::WARNING;
          log_flag = "WARN";
          break;
        }

        case LogLevel::kError : {
          level  = google::ERROR;
          log_flag = "ERROR";
          break;
        }
        case LogLevel::kFatal : {
          level  = google::FATAL;
          log_flag = "FATAL";
          break;
        }

        default : {
          level = google::INFO;
          log_flag = "NONE";
          break;
        }
      }

      const char * file = "";
      uint16_t line = 0;
      if (message->location.within_location) {
        file = message->location.file.c_str();
        line = message->location.line;
      }

      auto log = google::LogMessage(file, line, level);
      if (message->location.within_location){
        log.stream() << "[" << log_flag << "] " << "(" << message->location.function << ") " << "[" << message->context_id << "]: ";
      } else{
        log.stream() << "[" << log_flag << "] " << "[" << message->context_id << "]: ";
      }
      
      for (auto const& var : *message->variables.load()) {
        const_cast<PlatformLogMessagePtr &>(message)->show_var(log.stream(), *var);
      }
    }

    virtual void flush(){}
  } glog_customer;

  bool Initialize(const char * applicationId,
                  const char * applicationDescription,
                  LogLevel const defaultLogLevel, LogMode const mode,
                  const char * filePath) noexcept override {
    (void) applicationId;
    (void) applicationDescription;
    (void) defaultLogLevel;
    (void) mode;
    (void) filePath;
    if(pid_ < 0) {
      NOEXCEPT_CERR( << "[adsp-log:glog pulgin]: This process may be a child process after fork."
        << " This process will not be able to use adsp-log:glog pulgin. pid ->" << getpid() << std::endl);
      return false;
    }

    if(filePath == nullptr){
      NOEXCEPT_CERR( << "You must specify a log file path, before log system work" << std::endl);
      return false;
    }
    try {
    
      is_initialized = true;
      pid_ = getpid();

      /*-----------log file manager--------------*/
      GlogFileLimit limit;
      std::string file = std::string(filePath);
      std::string dir = hanhai::log::ParseDir(file);
      file = file.substr(dir.size() + 1);
      
      uint32_t default_max_file_num = 10;
      uint32_t default_flush_interval = 5;
      uint32_t default_file_block_size = 8; // 8MB

      if (hanhai::log::ParseProtoConfig(std::string(PLATFORM_LOG_CONFIG_DIR)+"/glog.conf", limit))
      {
        if(!limit.configs().empty()) {
          for(auto& conf : limit.configs()) {
            if(conf.log_name() == file) {
              if(conf.has_log_file_max_day()) {
                limit.set_log_file_max_day(conf.log_file_max_day());
              }

              if(conf.has_log_file_max_size()){
                limit.set_log_file_max_size(conf.log_file_max_size());
              }

              if(conf.has_max_file_num()) {
                limit.set_max_file_num(conf.max_file_num());
              }

              if(conf.has_flush_interval()) {
                limit.set_flush_interval(conf.max_file_num());
              }
              
              break;
            }
          }
        }
        FLAGS_max_log_size = limit.log_file_block_size();
        
      } else {
        FLAGS_max_log_size = default_file_block_size;
      }

      /****************** set default config ********************/
      // NOTE: log_file_max_day and log_file_max_size no default limit value
      if(!limit.has_max_file_num()) {
        limit.set_max_file_num(default_max_file_num);
      }

      if(!limit.has_flush_interval()) {
        limit.set_flush_interval(default_flush_interval);
      }
      /****************** set default config over ***************/

      FLAGS_logbufsecs = limit.flush_interval();

      if(limit.has_max_file_num() && limit.max_file_num() == 0) {
        NOEXCEPT_CERR( << "max_file_num set to 0, it will work by default value: " << default_max_file_num << std::endl);
        limit.set_max_file_num(default_max_file_num);
      }

      auto log_manager_task = [this, limit](LogFiles files){
        if(limit.has_log_file_block_size() && limit.has_log_file_max_size()){
          if(limit.log_file_block_size() * 1024 * 1024 >= limit.log_file_max_size()){
            NOEXCEPT_CERR( << "[adsp-log:glog plugin]: " << "error! 'log_file_block_size("
              << limit.log_file_block_size() << " MB) >= log_file_max_size(" 
              << limit.log_file_max_size() << " Byte)' has meet." << std::endl);
            return;
          }
        }
        DeleteLogfile(limit, files);
      };

      auto file_name_generator = [this, log_manager_task, dir, file](const std::string name) -> std::string {
        LogFiles log_files;
        GetLogFiles(dir, file, log_files);
        log_manager_task(log_files);

        static uint32_t global_file_num = 1;
        if(log_files.empty()){
          global_file_num = 1;
        } else {
          global_file_num = log_files.rbegin()->first + 1;
        }

        return name + "[" + std::to_string(global_file_num) + "]";
      };
      /*-----------log file manager--------------*/

      _flie_glogger = new google::LogFileObject(file_name_generator, filePath);

      _flie_glogger->SetSymlinkBasename(file.c_str());

      google::base::SetLogger(google::INFO, _flie_glogger);
      google::InitGoogleLogging(applicationId);
      google::SetLogDestination(google::INFO, filePath);
      google::SetLogDestination(google::ERROR, "");
      google::SetLogDestination(google::WARNING, "");
      google::SetLogDestination(google::FATAL, "");

      customer = &glog_customer;
      return true;
    } catch (std::exception& e) {
      NOEXCEPT_CERR( << "glog-logger init faild: " << e.what());
      return false;
    }
  }

  void Deinitialize() noexcept override {
    if(!is_initialized) return;
    is_initialized = false;
    if(getpid() == pid_) {
      // TODO: disable log_manager_task
    } else {
      pid_ = -1;
      NOEXCEPT_COUT( << "[adsp-log:glog pulgin]: This process may be a child process after fork."
        << " This process will deinitialize glog plugin. pid ->" << getpid() << std::endl);
    }
    
    customer->Deinitialize();
    customer = nullptr;
    delete _flie_glogger;
    _flie_glogger = nullptr;
  }

 private:
  int pid_ = 0;
  google::LogFileObject * _flie_glogger = nullptr;
};

hanhai::log::ThirdpartyLogger* CreateThirdpartyLogger() {
  return new GlogLogger();
}