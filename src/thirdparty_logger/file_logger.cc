/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/

#include "hanhai-platform-log/version/version.h"
#include "hanhai-platform-log/thirdparty_logger_impl.h"

#include <iostream>
#include <mutex>
#include <fstream>
#include <sstream>
#include <hanhai-platform-log/iostream_wrapper.h>

#define MAX_BUFF_SIZE 10 // kb
#define DEFAULT_FILE_NAME "hanhai-platform-log.log"

using namespace hanhai::log;

class FileLogger : public ThirdpartyLogger {
 public:
  ~FileLogger() { Deinitialize(); }
  class FileCustomer : public LogCustomer {
   public:
    FileCustomer(std::string filename) : LogCustomer(false, false), file_path_(filename) {
      if(file_path_.empty()) file_path_ = DEFAULT_FILE_NAME;
      file_stream_.open(file_path_, std::fstream::out | std::fstream::app);
    }
    
    virtual ~FileCustomer() {
      LogCustomer::Deinitialize();
      try{
        file_stream_.close();
      } catch (std::exception& e) {
        NOEXCEPT_CERR(<< "close file failed: " << e.what() << std::endl;);
      }
    }
    virtual void Consume(PlatformLogMessagePtr const & message) override{
      if(!is_running_) return;
      
      file_stream_ << *message << std::endl;
    }

    virtual void flush(){
      if(stringbuff.str().empty()) return;
      file_stream_ << stringbuff.str();
      file_stream_.flush();
      stringbuff.str("");
    }

    std::stringstream stringbuff;
    std::fstream file_stream_;
    std::string file_path_;
  };

  std::shared_ptr<FileCustomer> file_customer;

  bool Initialize(const char * applicationId,
                  const char * applicationDescription,
                  LogLevel const defaultLogLevel, LogMode const mode,
                  const char * filePath) noexcept override {
    (void) applicationId;
    (void) applicationDescription;
    (void) defaultLogLevel;
    (void) mode;
    (void) filePath;
    
    is_initialized = true;
    try {
      file_customer = std::make_shared<FileCustomer>(std::string(filePath)+".log");
      customer = file_customer.get();
      return true;
    } catch (std::exception& e) {
      NOEXCEPT_CERR(<< "Initialize file log failed: " << e.what() << std::endl);
      return false;
    }
  }

  void Deinitialize() noexcept override {
    if(!is_initialized) return;
    is_initialized = false;
    customer->Deinitialize();
    customer = nullptr;
  }
};

hanhai::log::ThirdpartyLogger* CreateThirdpartyLogger() {
  return new FileLogger();
}