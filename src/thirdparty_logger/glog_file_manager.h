/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

#include <string>
#include <iostream>
#include <map>
#include <tuple>

#include "hanhai-platform-log/file.h"

#include <glog_conf.pb.h>

namespace hanhai {
namespace log {
struct LogFileAttr
{
  std::string name;
  long size;
  long ts;
};

using LogFiles = std::map<uint32_t, LogFileAttr>;

#define MAX_NAME_SIZE 1024

bool GetLogFiles(const std::string dir, const std::string file_name, LogFiles &log_files);
bool DeleteLogfile(GlogFileLimit limit, LogFiles log_files);
}  // namespace impl
}  // namespace log