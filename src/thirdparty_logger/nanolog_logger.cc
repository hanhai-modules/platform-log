/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/

#include "hanhai-platform-log/version/version.h"
#include "hanhai-platform-log/thirdparty_logger_impl.h"

#include <iostream>
#include <mutex>
#include <sstream>

#include <Bstlog/Nanolog/NanoLogCpp17.h>
#include <Bstlog/log.h>
#include "hanhai-platform-log/iostream_wrapper.h"

using namespace hanhai::log;

class NanologLogger : public ThirdpartyLogger {
 public:
  ~NanologLogger() { Deinitialize(); }
  class GlogCustomer : public LogCustomer {
   public:
    GlogCustomer() : LogCustomer(true, false) {};
    virtual ~GlogCustomer() {
      LogCustomer::Deinitialize();
    }
    virtual void Consume(PlatformLogMessagePtr const & message) override{
      const char * file = "";
      uint16_t line = 0;
      if (message->location.within_location) {
        file = message->location.file.c_str();
        line = message->location.line;
        stringbuff << "[" << file <<  ":" << line << "]: ";
      }

      for (auto const& var : *message->variables.load()) {
        const_cast<PlatformLogMessagePtr &>(message)->show_var(stringbuff, *var);
      }

      // int level;
      static NanoLogInternal::ParamType *paramTypes = new NanoLogInternal::ParamType[1]{(NanoLogInternal::ParamType)-1};
      static int logId = -1;
      switch(message->log_level){
        case LogLevel::kDebug : {
          // level  = 4;
          NanoLogInternal::log(logId,
                          "",
                          line,
                          4,
                          "%s",0,paramTypes, stringbuff.str().c_str());
          break;
        }

        case LogLevel::kInfo : {
          // level  = 3;
          NanoLogInternal::log(logId,
                          "",
                          line,
                          3,
                          "%s",0,paramTypes, stringbuff.str().c_str());
          break;
        }

        case LogLevel::kWarn : {
          // level  = 2;
          NanoLogInternal::log(logId,
                          "",
                          line,
                          2,
                          "%s",0,paramTypes, stringbuff.str().c_str());
          break;
        }

        case LogLevel::kError :
        case LogLevel::kFatal : {
          // level  = 1;
          NanoLogInternal::log(logId,
                          "",
                          line,
                          1,
                          "%s",0,paramTypes, stringbuff.str().c_str());
          break;
        }

        default : {
          // level  = 3;
          NanoLogInternal::log(logId,
                          "",
                          line,
                          3,
                          "%s",0,paramTypes, stringbuff.str().c_str());
          break;
        }
      }
      
      stringbuff.clear();
      stringbuff.str("");
      // NanoLog::sync();
    }

    virtual void flush(){}

    std::stringstream stringbuff;
    
  } nanolog_customer;

  bool Initialize(const char * applicationId,
                  const char * applicationDescription,
                  LogLevel const defaultLogLevel, LogMode const mode,
                  const char * filePath) noexcept override {
    (void) applicationId;
    (void) applicationDescription;
    (void) defaultLogLevel;
    (void) mode;
    (void) filePath;
    if(filePath == nullptr){
      std::cout << "Please specify log file path. before that, the log system while not work." << std::endl;
      return false;
    }
    try {
      NanoLog::setLogFile(filePath);

      switch(defaultLogLevel){
        case LogLevel::kDebug : {
          NanoLog::setLogLevel(NanoLog::LogLevels::LogLevel::DEBUG);
          break;
        }

        case LogLevel::kInfo : {
          NanoLog::setLogLevel(NanoLog::LogLevels::LogLevel::INFO);
          break;
        }

        case LogLevel::kWarn : {
          NanoLog::setLogLevel(NanoLog::LogLevels::LogLevel::WARNING);
          break;
        }

        case LogLevel::kError :
        case LogLevel::kFatal : {
          NanoLog::setLogLevel(NanoLog::LogLevels::LogLevel::ERROR);
          break;
        }

        default : {
          NanoLog::setLogLevel(NanoLog::LogLevels::LogLevel::INFO);
          break;
        }
      }
      

      is_initialized = true;
      customer = &nanolog_customer;
      return true;
    } catch(std::exception& e) {
      NOEXCEPT_CERR(<< "nanolog init failed: " << e.what() << std::endl);
      return false;
    }
  }

  void Deinitialize() noexcept override {
    if(!is_initialized) return;
    is_initialized = false;
    customer->Deinitialize();
    NanoLog::sync();
    customer = nullptr;
  }
};

hanhai::log::ThirdpartyLogger* CreateThirdpartyLogger() {
  return new NanologLogger();
}
