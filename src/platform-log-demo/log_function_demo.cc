/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/logger.h"
#include <string.h>


using namespace hanhai::log;
int main(int argc, char** argv){
    
    INIT_LOG_SYSTEM(LogLevel::kDebug, LogMode::kConsole|LogMode::kFile, "log_function_demo/log_function_demo");

    ADEBUG << "This is a demo for log function";
    AWARN << "This is a demo for log function";
    AERROR << "This is a demo for log function";

    AWARN << "hex: " << Fmt::kHex << 0x10 << ", bin: " << Fmt::kBin << (int64_t)0x10 << ", dec: " << Fmt::kDec << 0x10;
    LOG_WITH_LOCATION("function_test", LogLevel::kInfo)  << "This is a demo for log function";
    
    usleep(1000000);
    
    Logger::Deinitialize();
    return 0;
}