cmake_minimum_required(VERSION 3.10)

project(hanhai-platform-log)
include(GNUInstallDirs)

add_executable(log_function_demo log_function_demo.cc)
target_link_libraries(log_function_demo hanhai-platform-log)

install(TARGETS log_function_demo
RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/examples/platform-log-demo/)
