/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/global_config.h"

#include <fstream>
#include <iostream>

#include "platform_log_config.pb.h"

#include "config.h"
#include "hanhai-platform-log/file.h"
#include "hanhai-platform-log/iostream_wrapper.h"

namespace hanhai {
namespace log {

using namespace hanhai::log::proto;



bool GlobalConfiguration::init() {
  if (is_initialized) return false;

  try {
    PlatformLogConfig config;
    if(!ParseProtoConfig(PLATFORM_LOG_CONFIG_PATH, config)){
      return false;
    }

    std::string lib_root = config.thirdparty_logger_path_root();
    if (lib_root.empty()) {
      return false;
    }

    thirdparty_root_directory = lib_root;

    for (auto& thirdparty_logger_conf : config.thirdparty_logger_confs()) {
      ThirdpartyLoggerConf conf;
      conf.name = thirdparty_logger_conf.name();
      conf.path = lib_root + "/" + thirdparty_logger_conf.path();
      thirdparty_logger_confs.push_back(conf);
    }
    return true;

  } catch(std::exception& e) {
    NOEXCEPT_CERR(<< "GlobalConfiguration::init failed: " << e.what() << std::endl);
    return false;
  }
}

void GlobalConfiguration::deinit() {
  if (!is_initialized) return;
  is_initialized = false;
  thirdparty_logger_confs.clear();
}

GlobalConfiguration::ThirdpartyLoggerConfs
    GlobalConfiguration::thirdparty_logger_confs = {};
bool GlobalConfiguration::is_initialized = false;
std::string GlobalConfiguration::thirdparty_root_directory = "";

}  // namespace log
}  // namespace hanhai