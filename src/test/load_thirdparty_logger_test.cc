/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/global_config.h"
#include "../impl/thirdparty_logger_loader.h"
#include "hanhai-platform-log/log_type.h"

#include <iostream>

int main(void){
    using namespace hanhai::log;
    GlobalConfiguration::init();
    auto confs = GlobalConfiguration::get_configurations();
    for(auto & conf : confs){
        std::cout << "name: " << conf.name << ", path: " << conf.path << std::endl;
        ThirdpartyLogger * logger = load_thirdparty_logger(conf.path);
        if(logger){
            std::cout << "load_thirdparty_logger success" << std::endl;
            if(logger->Initialize("", "", LogLevel::kDebug, LogMode::kConsole, "")){
                std::cout << "Initial success" << std::endl;
            }

            delete logger;
        }
    }
    return 0;
}