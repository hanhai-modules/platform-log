/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/logger.h"


using namespace hanhai::log;
int main(void){
    Logger::Initialize("fork_test", "fork_test for ads logger", LogLevel::kDebug, LogMode::kConsole, "log_fork_test.log");

    auto logger = Logger::CreateLogger("fork_test", "fork_test for ads logger", LogLevel::kInfo);
    for(int i = 0; i < 5; i++){
        logger.LogInfo()  << "This is a test for log function";
        sleep(1);
    }

    auto pid = fork();
    if(pid == 0){
        for(int i = 0; i < 10; i++){
            logger.LogInfo()  << "This is a test for log fork, from child process";
            sleep(1);
        }
    }else{
        for(int i = 0; i < 10; i++){
            logger.LogInfo().WithLocation(__FILE__, __LINE__, __FUNCTION__)  << "This is a test for log fork, from parent process";
            sleep(1);
        }
    }

    Logger::Deinitialize();
    return 0;

}