/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include <glog/logging.h>
#include "glog/raw_logging.h"

#include <iostream>
#include <string>
#include <unistd.h>

int main(const int argc, const char ** argv){
    std::string filePath = "./glog_file_test";
    google::InitGoogleLogging(argv[0]);
    google::SetLogDestination(google::ERROR, "");
    google::SetLogDestination(google::WARNING, "");
    google::SetLogDestination(google::FATAL, "");
    google::SetLogDestination(google::INFO, filePath.c_str());
    FLAGS_max_log_size = 1;

    int line = 0;
    google::LogSeverity level = google::GLOG_INFO;
    const char* file = "";
    for(int j = 0; j < 10000; ++j) {
        auto log = google::LogMessage(file, line, level);
        for(int i = 0; i < 100 ; ++i) {
            // if(i % 100 == 0) {
            //     google::FlushLogFiles(level);
            // }
            log.stream() << "log test" << std::endl;
            // log.Flush();
            usleep(10);
        }
    }
}

