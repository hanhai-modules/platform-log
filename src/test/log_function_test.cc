/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/logger.h"
#include <string.h>


using namespace hanhai::log;
int main(int argc, char** argv){
    bool loop = false;
    int sleep_time = 1000*1000;
    hanhai::log::LogMode mode = LogMode::kFile | LogMode::kConsole;
    if(argc >= 2 && strcmp(argv[1], "loop") == 0){
        loop = true;
    }

    if(argc >= 3 && 0 <= atoi(argv[2])) {
        std::cout << "argv[2]:" << argv[2] << std::endl;
        sleep_time = atoi(argv[2]);
        if(sleep_time <= 0) {
            std::cout << "argv 2: [" << argv[2] << "] not a positive integer" << std::endl;
            return 1;
        }
    } else {
        sleep_time = 1000;
        mode = LogMode::kFile;
    }
    
    INIT_LOG_SYSTEM(LogLevel::kDebug, mode, "log_function_test");

    for(int i = 0; loop || i < 10; i++){
        AWARN << "This is a test for log function";
        AWARN << "hex: " << Fmt::kHex << 0x10 << ", bin: " << Fmt::kBin << (int64_t)0x10 << ", dec: " << Fmt::kDec << 0x10;
        // LOG_WITH_LOCATION("function_test", LogLevel::kInfo)  << "This is a test for log function";
        
        usleep(sleep_time);
    }

    return 0;
}