/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "src/impl/log_impl.h"

#include <memory.h>
#include <stdlib.h>
#include <unistd.h>
#include <hanhai-platform-log/iostream_wrapper.h>
#include <iostream>

using namespace hanhai::log;

class MyLogCustomer : public LogCustomer {
 public:
  virtual void Consume(PlatformLogMessagePtr const& message) override {
    (void)message;
#if USE_BUILTIN_LOG_SYS
    static uint64_t last_seq_ = 0;
    if (last_seq_ > message->seq_) {
      NOEXCEPT_COUT( << "seq error: last: " << last_seq_ << ", now: " << message->seq_ << std::endl);  
    }
#endif
    inner_msg_counter_.fetch_add(1, std::memory_order_seq_cst);
  }
  virtual ~MyLogCustomer() { LogCustomer::Deinitialize(); }
  MyLogCustomer() : LogCustomer(true, true) {};

  static std::atomic_uint64_t inner_msg_counter_;
};

std::atomic_uint64_t MyLogCustomer::inner_msg_counter_ = {0};
MyLogCustomer* consumer = new MyLogCustomer();

bool Initialize(std::string const applicationId,
                std::string const applicationDescription,
                LogLevel const defaultLogLevel, LogMode const mode,
                std::string const filePath) {
  if (impl::LoggerImpl::is_running_.load(std::memory_order_release)) {
    return false;
  }
  (void)defaultLogLevel;
  (void)mode;
  impl::LoggerImpl::default_log_level_ = defaultLogLevel;
  impl::LoggerImpl::file_path_ = filePath;
  impl::LoggerImpl::ApplicationId_ = applicationId;
  impl::LoggerImpl::ApplicationDescription_ = applicationDescription;

  impl::LoggerImpl::RegisterCustom(consumer);
  impl::LoggerImpl::register_handler();

  return impl::LoggerImpl::init();
}

#define TASK_NUM 100
#define MSG_COUNTER 20000
#define MSG_INTERVAL 2000   // us
#define TASK_INTERVAL 1000  // us

int main(void) {
  bool init = Initialize("log_impl_test", "log_impl_test", LogLevel::kInfo,
                         LogMode::kFile, "./log_impl_test.log");

  if (!init) {
    NOEXCEPT_COUT( << "init failed" << std::endl);
    return -1;
  }
  NOEXCEPT_COUT( << "size of message: " << sizeof(PlatformLogMessage) << std::endl);
  sleep(1);
  PlatformLogMessage** msgs[TASK_NUM];

  auto task = [](int index, PlatformLogMessage** msgs_[TASK_NUM]) {
    usleep(100);
    auto logger = new impl::LoggerImpl(
        "loggerimpl test_" + std::to_string(index),
        "test the impl of logger, for customer", LogLevel::kInfo);

    auto msg = std::make_shared<PlatformLogMessage>();
    PlatformLogVariable* var = new PlatformLogVariable();
    var->buff = new uint8_t[var->dlt_size + var->data_size + var->unit_size];
    std::string unit = "m/s";
    uint32_t data = 100;

    var->dlt_size = 0;
    var->data_type = ADSDataType::ADS_UINT32;
    var->unit_size = unit.size();
    var->data_size = sizeof(data);

    memcpy(var->buff + var->dlt_size, unit.c_str(), var->unit_size);
    memcpy(var->buff + +var->unit_size, &data, var->data_size);

    msg->push(var);
    for (int i = 0; i < MSG_COUNTER; i++) {
      msg->log_level = LogLevel::kInfo;
      logger->push_message(msg);
      std::this_thread::sleep_for(std::chrono::microseconds(MSG_INTERVAL));
    }
  };

  std::thread* t[TASK_NUM] = {nullptr};

  auto start_time = std::chrono::system_clock::now();

  try {
    for (int num = 0; num < TASK_NUM; num++) {
      t[num] = new std::thread(task, num, msgs);
      usleep(TASK_INTERVAL);
    }

    for (int num = 0; num < TASK_NUM; num++) {
      t[num]->join();
      delete t[num];
    }
  } catch (std::exception& e) {
    impl::LoggerImpl::deinitialized();
    delete consumer;
    NOEXCEPT_CERR( << "test failed: " << e.what() << std::endl);
    return 1;
  }

  NOEXCEPT_COUT( << "send over. task number: " << TASK_NUM << std::endl);

  impl::LoggerImpl::deinitialized();
  delete consumer;
  NOEXCEPT_COUT( << "deinitialized over: " << TASK_NUM << std::endl);
  auto end_time = std::chrono::system_clock::now();

  int send_msg = MSG_COUNTER * TASK_NUM;
  NOEXCEPT_COUT( << "MSG_COUNTER in preset: : " << send_msg << std::endl);
  NOEXCEPT_COUT( << "MSG_COUNTER in consumer: : "
            << consumer->inner_msg_counter_.load() << std::endl);

  NOEXCEPT_COUT( << "lost : " << send_msg - consumer->inner_msg_counter_.load()
            << std::endl);
  auto time_cost = std::chrono::duration_cast<std::chrono::milliseconds>(
      end_time - start_time);
  auto real_time_cost =
      time_cost.count() -
      (100 + TASK_INTERVAL * TASK_NUM + MSG_COUNTER * MSG_INTERVAL) / 1000.0;
  NOEXCEPT_COUT( << "cost time: " << real_time_cost << " ms" << std::endl);
  NOEXCEPT_COUT( << "throughput: "
            << consumer->inner_msg_counter_.load() / 1000.0 / real_time_cost
            << " M/s" << std::endl);

  return 0;
}