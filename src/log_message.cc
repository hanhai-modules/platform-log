/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#include "hanhai-platform-log/log_message.h"

#include <memory.h>

#include <iomanip>
#include <iostream>
#include <bitset>

namespace hanhai {
namespace log {
#if USE_BUILTIN_LOG_SYS
std::atomic_uint64_t PlatformLogMessage::seq_counter_ {0};
#endif
PlatformLogMessage::PlatformLogMessage() {
  variables.store(new std::list<PlatformLogVariable const*>(),
                  std::memory_order_consume);
  variables.load()->clear();
  time_stamp_ = std::chrono::steady_clock::now();
#if USE_BUILTIN_LOG_SYS
  seq_ = seq_counter_.fetch_add(1, std::memory_order_seq_cst);
#endif
}

PlatformLogMessage::PlatformLogMessage(const PlatformLogMessage& other) {
  variables.store(new std::list<PlatformLogVariable const*>(),
                  std::memory_order_consume);

  for (auto var : *other.variables.load(std::memory_order_release)) {
    size_t buff_len = var->unit_size + var->data_size + var->dlt_size;
    uint8_t* buff = new uint8_t[buff_len];
    memcpy(buff, var->buff.load(), buff_len);

    auto my_var = new PlatformLogVariable();
    my_var->data_size = var->data_size;
    my_var->unit_size = var->unit_size;
    my_var->dlt_size = var->dlt_size;
    my_var->data_type = var->data_type;
    my_var->buff.store(buff, std::memory_order_consume);

    variables.load(std::memory_order_release)->push_back(my_var);
  }

  location = other.location;
  context_id = other.context_id;
  context_description = other.context_description;
  log_level = other.log_level;
  time_stamp_ = other.time_stamp_;
  current_fmt = other.current_fmt;
#if USE_BUILTIN_LOG_SYS
  seq_ = other.seq_;
#endif
}

PlatformLogMessage& PlatformLogMessage::operator=(PlatformLogMessage&& other) {
  variables.store(other.variables.load(std::memory_order_consume),
                  std::memory_order_consume);
  other.variables.store(nullptr, std::memory_order_consume);
  location = other.location;
  context_id = other.context_id;
  context_description = other.context_description;
  log_level = other.log_level;
  time_stamp_ = other.time_stamp_;
  current_fmt = other.current_fmt;
#if USE_BUILTIN_LOG_SYS
  seq_ = other.seq_;
#endif
  return *this;
}
PlatformLogMessage::~PlatformLogMessage() {
  auto tmp_variables = variables.load(std::memory_order_release);
  if (tmp_variables == nullptr) {
    return;
  }

  while (tmp_variables->size() > 0) {
    PlatformLogVariable const* variable = tmp_variables->front();
    tmp_variables->pop_front();
    delete variable;
  }

  delete tmp_variables;
}

void PlatformLogMessage::push(PlatformLogVariable const* variable) noexcept {
  auto tmp_variables = variables.load(std::memory_order_release);
  if (tmp_variables == nullptr) {
    return;
  }
  tmp_variables->push_back(variable);
}

PlatformLogVariable const* PlatformLogMessage::pop() noexcept {
  auto tmp_variables = variables.load(std::memory_order_release);
  if (tmp_variables == nullptr) {
    return nullptr;
  }
  if (tmp_variables->size() == 0) {
    return nullptr;
  }

  PlatformLogVariable const* variable = tmp_variables->back();
  tmp_variables->pop_back();

  return variable;
}

PlatformLogVariable::~PlatformLogVariable() {
  auto tmp_buff = buff.load(std::memory_order_release);
  if (tmp_buff) delete[] tmp_buff;
}

bool PlatformLogMessage::empty() {
  return (variables.load(std::memory_order_release))->empty();
}

std::ostream& operator<<(std::ostream& out, PlatformLogMessage const& message) {
  if (message.variables.load() == nullptr) return out;

  out << "[" << message.context_id << "] ";
  if (message.location.within_location) {
    out << "[" << message.location.file << ":" << message.location.line << "] ["
        << message.location.function << "]: ";
  }
#if USE_BUILTIN_LOG_SYS
  out << "[seq: " << message.seq_ << "] ";
#endif
  switch(message.log_level) {
    case LogLevel::kDebug:
      out << "[DEBUG]: ";
      break;
    case LogLevel::kInfo:
      out << "[INFO]: ";
      break;
    case LogLevel::kWarn:
      out << "[WARNING]: ";
      break;
    case LogLevel::kError:
      out << "[ERROR]: ";
      break;
    case LogLevel::kFatal:
      out << "[FATAL]: ";
      break;
    case LogLevel::kVerbose:
    default:
      out << "[UNKNOWN]: ";
      break;
  }
  for (auto const& var : *message.variables.load()) {
   const_cast<PlatformLogMessage &>(message).show_var(out, *var);
  }
  return out;
}

std::ostream& PlatformLogMessage::show_var(std::ostream& out, PlatformLogVariable const& var) {
  if (var.dlt_size != 0) return out;

  if (var.buff) {
    std::string unit;

    if (var.unit_size != 0)
      unit =
          std::string((char*)(var.buff.load() + var.dlt_size), var.unit_size);
    if (var.data_size != 0) {
      switch (var.data_type) {
        case ADSDataType::ADS_STRING: {
          std::string data = std::string(
              (char*)((void*)(var.buff.load() + var.dlt_size + var.unit_size)),
              var.data_size);
          out << data << unit;
          break;
        }

        case ADSDataType::ADS_BOOL: {
          bool data_bool =
              *(bool*)((void*)(var.buff.load() + var.dlt_size + var.unit_size));
          if (data_bool) {
            out << "true" << unit;
          } else {
            out << "false" << unit;
          }
          break;
        }

        case ADSDataType::ADS_INT8: {
          int8_t data_int8 = *static_cast<int8_t*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          if(current_fmt.fmt == Fmt::kBin)
            out << std::bitset<sizeof(int8_t)*8>(data_int8) << unit;
          else
            out << data_int8 << unit;
          break;
        }

        case ADSDataType::ADS_INT16: {
          int16_t data_int16 = *static_cast<int16_t*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          if(current_fmt.fmt == Fmt::kBin)
            out << std::bitset<sizeof(int16_t)*8>(data_int16) << unit;
          else
            out << data_int16 << unit;
          break;
        }

        case ADSDataType::ADS_INT32: {
          int32_t data_int32 = *static_cast<int32_t*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          if(current_fmt.fmt == Fmt::kBin)
            out << std::bitset<sizeof(int32_t)*8>(data_int32) << unit;
          else
            out << data_int32 << unit;
          break;
        }

        case ADSDataType::ADS_INT64: {
          int64_t data_int64 = *static_cast<int64_t*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          if(current_fmt.fmt == Fmt::kBin)
            out << std::bitset<sizeof(int64_t)*8>(data_int64) << unit;
          else
            out << data_int64 << unit;
          break;
        }

        case ADSDataType::ADS_UINT8: {
          uint8_t data_uint8 = *static_cast<uint8_t*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          if(current_fmt.fmt == Fmt::kBin)
            out << std::bitset<sizeof(uint8_t)*8>(data_uint8) << unit;
          else
            out << data_uint8 << unit;
          break;
        }

        case ADSDataType::ADS_UINT16: {
          uint16_t data_uint16 = *static_cast<uint16_t*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          if(current_fmt.fmt == Fmt::kBin)
            out << std::bitset<sizeof(uint16_t)*8>(data_uint16) << unit;
          else
            out << data_uint16 << unit;
          break;
        }

        case ADSDataType::ADS_UINT32: {
          uint32_t data_int32 = *static_cast<uint32_t*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          if(current_fmt.fmt == Fmt::kBin)
            out << std::bitset<sizeof(uint32_t)*8>(data_int32) << unit;
          else
            out << data_int32 << unit;
          break;
        }

        case ADSDataType::ADS_UINT64: {
          uint64_t data_int64 = *static_cast<uint64_t*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          if(current_fmt.fmt == Fmt::kBin)
            out << std::bitset<sizeof(uint64_t)*8>(data_int64) << unit;
          else
            out << data_int64 << unit;
          break;
        }

        case ADSDataType::ADS_FLOAT: {
          float data_float = *static_cast<float*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          out << data_float << unit;
          break;
        }

        case ADSDataType::ADS_DOUBLE: {
          double data_double = *static_cast<double*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          out << data_double << unit;
          break;
        }

        case ADSDataType::ADS_TIME: {
          time_t data_time = *static_cast<time_t*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          out << std::ctime(&data_time) << unit;
          break;
        }

        case ADSDataType::ADS_FORMAT: {
          Format format = *static_cast<Format*>(
              (void*)(var.buff.load() + var.dlt_size + var.unit_size));
          switch (format.fmt) {
            case Fmt::kDec: {
              out << std::dec;
              break;
            }

            case Fmt::kHex: {
              out << std::hex;
              break;
            }

            case Fmt::kOct: {
              out << std::oct;
              break;
            }

            case Fmt::kBin: {
              // out << std::binary;
              break;
            }

            case Fmt::kDecFloat:
            case Fmt::kHexFloat:
            case Fmt::kEngFloat:
            case Fmt::kAutoFloat: {
              out << std::fixed << std::setprecision(format.precision);
            }

            default:
              break;
          }

          this->current_fmt = format;
        }

        default:
          break;
      }
    }
  }
  return out;
}

std::ostream& PlatformLogMessage::show_var(std::ostream& out, PlatformLogVariablePtr var) {
  return out;
}
std::ostream& PlatformLogMessage::show_var(std::ostream& out, PlatformLogVariable const* var) {
  return out;
}

std::ostream& operator<<(std::ostream& out, PlatformLogMessage const* message) {
  return out;
}
std::ostream& operator<<(std::ostream& out, PlatformLogMessagePtr message) {
  return out;
}

}  // namespace log
}  // namespace hanhai