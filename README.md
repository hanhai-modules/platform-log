### Log system for bst ads-platform
This program is released under the Apache 2.0 License.
This product bundles glog_file_logger, which is available under a "Apache 2.0" license. For details, see src/thirdparty_logger/glog.

### USEAGE:

头文件 include <hanhai-platform-log/logger.h>

命名空间： hanhai::log

1. 简易接口

   a) 初始化日志系统：

   `INIT_LOG_SYSTEM(default_log_level, log_mode, "log_path")`， 其中：

   **default_log_level**：可以为 `LogLevel::kFatal、LogLevel::kError、 LogLevel::kWarn、LogLevel::kInfo、LogLevel::kDebug`, 代表日志等级的阈值；

   **log_mode：** 可以为 `LogMode::kFile、LogMode::kConsole`中的一种或者混合(`|`代表混用两种模式)；

   **log_path：** 为日志文件的储存路径（**不包含扩展名**）, 该路径会被自动添加前缀`/userdata/ADS/LOG`/，且会自动创建路径。

   **例**： `INIT_LOG_SYSTEM(LogLevel::kDebug, LogMode::kFile | LogMode::kConsole, "log_function_test");`

   b) 生成日志消息：

   ```c++
   ADEBUG << "some info" << ...;
   AINFO  << "some info" << ...;
   AWARN  << "some info" << ...;
   AERROR << "some info" << ...;
   AFATAL << "some info" << ...;
   ```

   ps: AWARN及以上的日志会带上文件名和行号。用该方式输出日志会以程序名作为模块名；文件名建议为相对路径文件夹名加文件名的形式：`dir_name/file_name`，避免你的日志文件和其他人的日志文件混在一起
   
2. 更灵活的接口

   a) 初始化日志系统：

   `Logger::Initialize("program name", "program desc", default_log_level, log_mode, "log_path")`, 其中：

   **default_log_level**：可以为 `LogLevel::kFatal、LogLevel::kError、 LogLevel::kWarn、LogLevel::kInfo、LogLevel::kDebug`, 代表日志等级的阈值；

   **log_mode：** 可以为 `LogMode::kFile、LogMode::kConsole`中的一种或者混合(`|`代表混用两种模式)；

   **log_path：** 为日志文件的储存路径（**不包含扩展名**）, 该路径会被自动添加前缀`/userdata/ADS/LOG`，且会自动创建路径。

   **例：** `Logger::Initialize("fork_test", "fork_test for ads logger", LogLevel::kDebug, LogMode::kConsole, "log_fork_test.log");`

   b) 生成日志消息：

   ```c++
   ADEBUG_MODULE(module) << "some info" << ...;
   AINFO_MODULE(module)  << "some info" << ...;
   AWARN_MODULE(module)  << "some info" << ...;
   AERROR_MODULE(module) << "some info" << ...;
   AFATAL_MODULE(module) << "some info" << ...;
   ```

   用该方式输出日志可以定位具体的模块名。

   PS: 两种风格的接口可以混用。

3. AUTOSAR风格的生成日志的接口
   ```c++
   auto logger = Logger::CreateLogger(module, "", level);
   logger.LogError() << "some info";
   logger.LogInfo().WithLocation(__FILE__, __LINE__, __FUNCTION__) << "some info";
   logger.WithLevel(level) << "some info";
   ```
   
4. 第三方日志系选择配置

   a) 配置文件：  `/etc/hanhai/conf/platform_log/platform_logger.conf`

   ``` protobuf
    thirdparty_logger_confs {
        name : "glog_logger" # 插件名称
        path : "libglog_logger.so" # 插件文件名
    }
    
    thirdparty_logger_path_root : "/usr/bin/hanhai-platform-log/plugin" # 插件根路径
   ```

   b) 环境变量`PLATFORM_LOG_IMPL`

   ​	通过环境变量`PLATFORM_LOG_IMPL`指向第三方日志实现的插件，如: `PLATFORM_LOG_IMPL=/usr/bin/hanhai-platform-log/plugin/libglog_logger.so`即可指定日志系统选用指定的日志实现（该方式会屏蔽配置文件的配置）

   c) 针对glog的日志文件管理配置

   ​	配置文件路径默认为`/etc/hanhai/conf/platform_log/glog.conf`。管理粒度为初始化日志系统传入的文件名（不包含文件夹名，cyber的初始化方式会以二进制名作为日志文件名）。配置的粒度为日志文件名，当未对特定文件名进行配置时，会使用配置的全局配置。当配置文件不存在（默认行为）或未进行任何配置时，默认不对日志文件进行任何管理。

   配置方法：

   如果不存在文件`/etc/hanhai/conf/platform_log/glog.conf`(具体路径会受cmake中的PLATFORM_LOG_PATH变量影响，不同的平台会不一样。给出的默认值在linux和qnx生效)，则创建一个新文件并填充以下内容：

   ```protobuf
   log_file_max_size : 83886080 #default config
   log_file_max_day : 30 #default config
   log_file_block_size : 10 # 10MB
   check_interval : 10 # 10 min
   flush_interval : 1 # 1 sec
   max_file_num : 10
   
   
   configs {
       log_name : "log_file_name";
       log_file_max_size : max_size;
       log_file_max_day : max_day;
       max_file_num : 10;
       flush_interval : 1 # 1 sec
   }
   ```

   -	其中最外层的log_file_max_size和log_file_max_day表示全局的默认配置，没有单独配置的日志文件会以这两个配置值进行日志文件管理。log_file_max_size表示某个程序能生成的日志文件的最大大小，log_file_max_day表示某个程序生成的日志文件的最大保留天数。**configs则可以配置多项**，其中log_name指定的日志文件会以特有的配置值来进行管理。

   -	当某个日志文件名对应的日志总大小超过配置值时，启动对应的程序（产生该日志文件的程序）后会删除更旧的日志，直到总的日志文件大小小于配置值（在程序运行过程中会持续的对生成的日志进行管理）。与之同时，生成日期早于配置的最大天数的日志也将会被删除（需要保证系统时间的稳定性，否则可能会误删）。

   -    `log_file_block_size`表示单个日志文件的最大大小(该配置值必须小于log_file_max_size， 否则可能导致正在生成的文件被删除)， 超过这个大小后会自动分包，单位为MB。而`check_interval`（已弃用）表示日志管理的周期，单位为分钟。
   
   -    `max_file_num`表示日志文件的最大个数，当超过这个个数时会删除旧的文件。
   
   -    `flush_interval`表示日志从内存flush到磁盘的周期。

  ps: adsp-log中的glog自定义了日志文件的生成规则，在原有的基础上增加了递增的序号，日志的新旧通过序号来进行区分。

5. 编译

    应用程序： 链接库`hanhai-platform-log`, CI依赖： `adsp-log`
    
    adsp-log库：
    
    ```shell
    # (一) A1000端：
    #   1. 创建build目录并cd到build目录。
    #   2. 进入sdk docker并source sdk_env
    #   3. 执行 cmake -DCMAKE_INSTALL_PREFIX=/usr ..
    #   4. make
    # A1000上配置文件的路径默认为/etc/hanhai/conf/platform_log/; plugin库的位置默认为/usr/bin/hanhai-platform-log/plugin；日志生成路径前缀为/userdata/ADS/LOG
    
    # (二) x86端：
    #   1. 创建build目录并cd到build目录。
    #   2. 执行cmake -DENABLE_NANOLOG=OFF -DENABLE_GLOG=OFF -DENABLE_DLTLOG=OFF -DPLATFORM_LOG_PATH=/opt/cyber/usr/bin/hanhai-platform-log/ -DPLATFORM_LOG_CONFIG_DIR=/opt/cyber/etc/hanhai/conf/platform_log -DLOG_PATH_PRTFIX=/opt/cyber/ ..
    #   3. make
    # x86上的配置文件路径默认为/opt/cyber/etc/hanhai/conf/platform_log; plugin库路径默认为/opt/cyber/usr/bin/hanhai-platform-log/plugin。
    # x86上目前没有移植glog和bstlog以及dlt, 目前是一个简单的写文件的file log；日志生成路径前缀为/opt/cyber/
    
    # ps: 编译时要确认定的路径中adsp-log的配置文件和plugin库是存在的， 不然日志系统会跑不起来。
    #    【PLATFORM_LOG_CONFIG_DIR】指定配置文件的路径；插件库的路径为【PLATFORM_LOG_PATH/plugin】；【LOG_PATH_PRTFIX】指定日志生成的路径前缀。
    ```
    
    
    
    
    
6. INSTALL

    通过`CMAKE_INSTALL_xxx`变量来控制安装路径的前缀(/usr/bin、/usr/lib)。但是如果编译时安装路径和部署时的安装路径不一致（如编译时安装到xxx/usr/bin/, 部署时安装到/opt/xxx/usr/bin/）,需要单独配置cmake变量`PLATFORM_LOG_PATH`为部署时的安装路径（如/opt/xxx/usr/bin/hanhai-platform-log）, `PLATFORM_LOG_CONFIG_DIR`配置为实际的配置文件的安装路径(/etc/xxx/xxx/)。以让adsp-log能够找到配置文件和插件。

ps:
    日志系统目前暂不支持`std::endl`作为换行符。

