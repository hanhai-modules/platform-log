/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#pragma GCC push_options
#pragma GCC optimize (0) //避免编译器优化
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable" //避免unused-variable warning
static const char *g_lib_ver=LIB_VERSION;
static const char *g_lib_git=GIT_VERSION;
#pragma GCC diagnostic pop
#pragma GCC pop_options

#pragma message(LIB_VERSION)
