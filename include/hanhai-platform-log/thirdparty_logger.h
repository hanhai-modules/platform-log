/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef __ADS_LOGGER_THIRDPARTY_LOGGER_H__
#define __ADS_LOGGER_THIRDPARTY_LOGGER_H__

#include "log_customer.h"
#include "log_message.h"

namespace hanhai {
namespace log {
class ThirdpartyLogger {
 public:
    virtual bool Initialize(const char * const applicationId,
                    const char * applicationDescription,
                    LogLevel const defaultLogLevel, LogMode const mode,
                    const char * filePath) noexcept = 0;
    virtual void Deinitialize() noexcept = 0;
    virtual ~ThirdpartyLogger() = default;
    bool is_initialized = false;
    LogCustomer *customer;
};

typedef ThirdpartyLogger* (*ThirdpartyLoggerProductor)();

} // namespace log
}  // namespace hanhai

#endif // __ADS_LOGGER_THIRDPARTY_LOGGER_H__