/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef PLATFORM_LOG_H
#define PLATFORM_LOG_H
#include <atomic>
#include <map>
#include <memory>
#include <string>

#include "config.h"
#include "log_customer.h"
#include "log_stream.h"
#include "log_type.h"

#ifndef PROGRAM_NAME
#if defined(__APPLE__) || defined(__FreeBSD__)
#define PROGRAM_NAME getprogname()
#elif defined(_GNU_SOURCE)
#define PROGRAM_NAME program_invocation_name
#elif defined(_WIN32)
#define PROGRAM_NAME __argv[0]
#elif defined(__QNXNTO__)
extern char * __progname;
#define PROGRAM_NAME __progname
#else
#define PROGRAM_NAME "?"
#endif
#endif // PROGRAM_NAME

#define INIT_LOG_SYSTEM(default_level, mode, log_file_path) \
    hanhai::log::Logger::Initialize(PROGRAM_NAME, "", default_level, mode, log_file_path)

#define ADEBUG ADEBUG_MODULE(PROGRAM_NAME)
#define AINFO AINFO_MODULE(PROGRAM_NAME)
#define AWARN AWARN_MODULE(PROGRAM_NAME)
#define AERROR AERROR_MODULE(PROGRAM_NAME)
#define AFATAL AFATAL_MODULE(PROGRAM_NAME)

#define ADEBUG_MODULE(module) LOG_WITHOUT_LOCATION(module, hanhai::log::LogLevel::kDebug)
#define AINFO_MODULE(module) LOG_WITHOUT_LOCATION(module, hanhai::log::LogLevel::kInfo)
#define AWARN_MODULE(module) LOG_WITH_LOCATION(module, hanhai::log::LogLevel::kWarn)
#define AERROR_MODULE(module) LOG_WITH_LOCATION(module, hanhai::log::LogLevel::kError)
#define AFATAL_MODULE(module) LOG_WITH_LOCATION(module, hanhai::log::LogLevel::kFatal)

#define LOG_WITH_LOCATION(module, level)  \
  hanhai::log::Logger::CreateLogger(module, "", level) \
      .WithLevel(level)                   \
      .WithLocation(__FILE__, __LINE__, __FUNCTION__)

#define LOG_WITHOUT_LOCATION(module, level) \
  hanhai::log::Logger::CreateLogger(module, "", level).WithLevel(level)

namespace hanhai {
namespace log {

namespace impl {
class LoggerImpl;
}

class Logger {
 public:
  ~Logger();
  Logger(Logger const&) = default;
  Logger& operator=(Logger const&) = default;

  static bool Initialize(std::string const applicationId,
                         std::string const applicationDescription,
                         LogLevel const defaultLogLevel, LogMode const mode,
                         std::string const filePath) noexcept;
  static bool Deinitialize() noexcept;
  static Logger& CreateLogger(std::string const contextId,
                              std::string const contextDescription,
                              LogLevel const severityLevel) noexcept;
  static void RegisterCustom(LogCustomer* consumer) noexcept;
  bool IsEnabled(LogLevel severityLevel) const noexcept;

  LogStream WithLevel(LogLevel const severityLevel) noexcept;
  // LogStream LogFatal() noexcept;
  LogStream LogError() noexcept;
  LogStream LogWarn() noexcept;
  LogStream LogInfo() noexcept;
  LogStream LogDebug() noexcept;
  // LogStream LogVerbose() noexcept;

  std::string context_id_;
  std::string context_description_;
  static std::string& ApplicationId_;
  static std::string& ApplicationDescription_;
  static LogLevel default_log_level_;

 protected:
  Logger() = default;

  LogLevel severity_level;
  impl::LoggerImpl* impl_;
  static std::map<const std::string, const std::shared_ptr<Logger>> loggers;
  static std::atomic_int
      loggers_ref;  // -1: blocking; 0: available; >0: reading
};

}  // namespace log
}  // namespace hanhai
#endif  // PLATFORM_LOG_H