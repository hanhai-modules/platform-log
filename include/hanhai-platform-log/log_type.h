/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef ADS_LOGTYPE_H
#define ADS_LOGTYPE_H
#include <stdint.h>

#define REMOTE_MODE  0x01 // 0001
#define FILE_MODE    0x02 // 0010
#define CONSOLE_MODE 0x04 // 0100
#define CUSTOM_MODE  0x08 // 1000

namespace hanhai {
namespace log {

enum class LogLevel : uint8_t {
            kOff = 0,
            kFatal = 1,
            kError = 2,
            kWarn = 3,
            kInfo = 4,
            kDebug = 5,
            kVerbose = 6,
};

class LogMode {
    public:
        LogMode(uint8_t mode) : bitset(mode){};
        LogMode operator|(LogMode other){
            return LogMode(this->bitset | other.bitset);
        }
        LogMode operator&(LogMode other){
            return LogMode(this->bitset & other.bitset);
        }
        bool operator==(LogMode other){
            return (this->bitset == other.bitset);
        }

        static LogMode kRemote;
        static LogMode kFile;
        static LogMode kConsole;
        static LogMode kCustom;
    private:
        uint8_t bitset;
};

enum class Fmt : uint16_t {
    kDefault = 0,
    kDec = 1,
    kOct = 2,
    kHex = 3,
    kBin = 4,
    kDecFloat = 5,
    kEngFloat = 6,
    kHexFloat = 7,
    kAutoFloat = 8,
};

struct Format {
    Fmt fmt;
    uint16_t precision;
    Format(Fmt fmt, uint16_t precision = 0) : fmt(fmt), precision(precision) {}
    Format() : fmt(Fmt::kDefault), precision(0) {}
};

} // namespace log
} // namespace hanhai

#endif // ADS_LOGTYPE_H