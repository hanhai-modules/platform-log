/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef __ADS_LOGGER_GLOBAL__CONFIG_H__
#define __ADS_LOGGER_GLOBAL__CONFIG_H__

#include <string>
#include <vector>


namespace hanhai {
namespace log {
class GlobalConfiguration {
 public:
  static bool init();
  static void deinit();

  struct ThirdpartyLoggerConf {
    std::string name;
    std::string path;
  };

  typedef std::vector<ThirdpartyLoggerConf> ThirdpartyLoggerConfs;

  static ThirdpartyLoggerConfs const& get_configurations() {
    return thirdparty_logger_confs;
  }
  static std::string thirdparty_root_directory;
 private:
  static ThirdpartyLoggerConfs thirdparty_logger_confs;
  static bool is_initialized;
};
}  // namespace log
}  // namespace hanhai

#endif  // __ADS_LOGGER_GLOBAL__CONFIG_H__