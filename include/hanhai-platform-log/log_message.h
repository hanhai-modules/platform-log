/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef PLATFORM_LOG_MESSAGE_H
#define PLATFORM_LOG_MESSAGE_H

#define PLATFORM_LOG_COMMAND_MAX_LEN 128
#define PLATFORM_LOG_ID_MAX_LEN 64
#define PLATFORM_LOG_DESCRIPTION_MAX_LEN 256

#include <stdint.h>
#include <list>
#include <memory>
#include <atomic>
#include <chrono>
#include <iostream>
#include <mutex>

#include "log_type.h"

namespace hanhai {
namespace log {

enum class ADSDataType : uint8_t {
  ADS_RAW = 0x00,
  ADS_BOOL = 0x01,
  ADS_UINT8 = 0x02,
  ADS_UINT16 = 0x03,
  ADS_UINT32 = 0x04,
  ADS_UINT64 = 0x05,
  ADS_INT8 = 0x06,
  ADS_INT16 = 0x07,
  ADS_INT32 = 0x08,
  ADS_INT64 = 0x09,
  ADS_FLOAT = 0x010,
  ADS_DOUBLE = 0x11,
  ADS_STRING = 0x12,
  ADS_FORMAT = 0x13,
  ADS_TIME = 0x14,
  ADS_DLT = 0x15,
  PLATFORM_LOG_COMMAND = 0x16
};

enum class PlatformLogCommandType : uint8_t {
  PLATFORM_LOG_ADD_CONTENT_DESC = 0x01
};

struct ADSPlatformLogCommand {
  PlatformLogCommandType command_type;
  uint8_t command[PLATFORM_LOG_COMMAND_MAX_LEN];
  size_t command_len;
};

struct PlatformLogVariable {
  std::atomic<uint8_t*> buff = {nullptr}; //  = | dlt | unit | data |
  size_t unit_size = 0;
  size_t data_size = 0;
  size_t dlt_size = 0;
  ADSDataType data_type = ADSDataType::ADS_RAW;
  ~PlatformLogVariable();
};

struct PlatformLogMessage {
  PlatformLogMessage(const PlatformLogMessage& other);
  PlatformLogMessage& operator=(PlatformLogMessage&& other);
  PlatformLogMessage();
  ~PlatformLogMessage();

  void push(PlatformLogVariable const * variable) noexcept;
  PlatformLogVariable const* pop() noexcept;
  bool empty();

  LogLevel log_level;
  std::string context_id;
  std::string context_description;
  struct Location {
    std::string file;
    uint16_t line;
    std::string function;
    bool within_location = false;
  } location;
  std::chrono::_V2::steady_clock::time_point time_stamp_;
  std::atomic<std::list<PlatformLogVariable const *>*> variables;
  Format current_fmt;
#if USE_BUILTIN_LOG_SYS
  uint64_t seq_ = 0;
  static std::atomic_uint64_t seq_counter_;
#endif
  using PlatformLogVariablePtr = std::shared_ptr<PlatformLogVariable>;
  std::ostream& show_var(std::ostream& out, PlatformLogVariablePtr var);
  std::ostream& show_var(std::ostream& out, PlatformLogVariable const * var);
  std::ostream& show_var(std::ostream& out, PlatformLogVariable const  & var);
};
using PlatformLogMessagePtr = std::shared_ptr<PlatformLogMessage>;

std::ostream& operator<<(std::ostream& out, PlatformLogMessage const & message);
std::ostream& operator<<(std::ostream& out, PlatformLogMessage const * message);
std::ostream& operator<<(std::ostream& out, PlatformLogMessagePtr message);

}  // namespace log
}  // namespace hanhai

#endif  // PLATFORM_LOG_MESSAGE_H