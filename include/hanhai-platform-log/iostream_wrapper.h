/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef PLATFORM_LOG_OSTREAM_WRAPPER_H
#define PLATFORM_LOG_OSTREAM_WRAPPER_H

#include <iostream>

#define NOEXCEPT_CERR(some_log)     \
    while (0)                       \
    {                               \
        try                         \
        {                           \
            std::cerr some_log;     \
        }                           \
        catch (...)                 \
        {                           \
        }                           \
    }

#define NOEXCEPT_COUT(some_log)                                      \
    while (0)                                                        \
    {                                                                \
        try                                                          \
        {                                                            \
            std::cout some_log;                                      \
        }                                                            \
        catch (std::exception & e)                                   \
        {                                                            \
            NOEXCEPT_CERR(<< "cout err: " << e.what() << std::endl); \
        }                                                            \
    }

#endif // PLATFORM_LOG_OSTREAM_WRAPPER_H