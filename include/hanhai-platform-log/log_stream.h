/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef PLATFORM_LOG_STREAM_H
#define PLATFORM_LOG_STREAM_H

#include <memory>
#include <sstream>

#include "log_type.h"


namespace hanhai {
namespace log {

namespace impl {
class LoggerImpl;
}
class Logger;

class LogStream final {
 public:
  friend class Logger;
  friend std::ostream& operator<<(std::ostream& out,
                                  LogStream& stream) noexcept;
  LogStream& operator<<(bool value) noexcept;
  LogStream& operator<<(uint8_t value) noexcept;
  LogStream& operator<<(uint16_t value) noexcept;
  LogStream& operator<<(uint32_t value) noexcept;
  LogStream& operator<<(uint64_t value) noexcept;
  LogStream& operator<<(int8_t value) noexcept;
  LogStream& operator<<(int16_t value) noexcept;
  LogStream& operator<<(int32_t value) noexcept;
  LogStream& operator<<(int64_t value) noexcept;
  LogStream& operator<<(float value) noexcept;
  LogStream& operator<<(double value) noexcept;
  LogStream& operator<<(std::string value) noexcept;
  LogStream& operator<<(const char* value) noexcept;
  LogStream& operator<<(char value) noexcept;
  LogStream& operator<<(const void* const value) noexcept;
  LogStream& operator<<(Format format) noexcept;
  LogStream& operator<<(Fmt fmt) noexcept;

  template <typename Rep, typename Period>
  LogStream& operator<<(
      const std::chrono::duration<Rep, Period>& value) noexcept {
    if(!enabled) return *this;
    return *this << std::chrono::duration_cast<std::chrono::nanoseconds>(value)
                        .count();
  }

  LogStream& operator<<(std::chrono::nanoseconds nanosec) noexcept;

  template <typename T>
  LogStream& operator<<(const std::shared_ptr<T>& value) noexcept {
    if(!enabled) return *this;
    return *this << value.get();
  }

  template <typename T>
  LogStream& operator<<(const std::unique_ptr<T>& value) noexcept {
    if(!enabled) return *this;
    return *this << value.get();
  }

  template <typename MessageT>
  LogStream& operator<<(MessageT message) noexcept {
    if(!enabled) return *this;
    std::stringstream tmp_stream;
    *this << tmp_stream.str();
    return *this;
  };
  // reentrant : sends out the current log buffer and initializes a new meessage
  // stream
  void flush() noexcept;
  ~LogStream() noexcept;
  LogStream& WithLocation(std::string file, uint16_t line) noexcept;
  LogStream& WithLocation(std::string file, uint16_t line,
                          std::string func) noexcept;
  LogStream(LogStream&& other);

 private:
  LogStream(LogLevel const severityLevel);
  LogStream& operator=(LogStream&& other);
  PlatformLogMessagePtr message_;
  impl::LoggerImpl* log_impl_;
  bool inner_flush_ = false;
  bool enabled = false;
};

std::ostream& operator<<(std::ostream& out, LogStream& stream) noexcept;

}  // namespace log
}  // namespace hanhai

#endif  // PLATFORM_LOG_STREAM_H