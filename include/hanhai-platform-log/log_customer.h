/*
Copyright (c) 2024 Black Sesame Technologies
*
*Licensed under the Apache License, Version 2.0 (the "License");
*you may not use this file except in compliance with the License.
*You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
*Unless required by applicable law or agreed to in writing, software
*distributed under the License is distributed on an "AS IS" BASIS,
*WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*See the License for the specific language governing permissions and
*limitations under the License.
*/


#ifndef PLATFORM_LOG_CUSTOMER_H
#define PLATFORM_LOG_CUSTOMER_H

#include "log_message.h"

#include <list>
#include <vector>
#include <thread>
#include <unistd.h>
#include <semaphore.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>

#include "config.h"

#define CUSTOMER_FLUSH_INTER 20 // ms

namespace hanhai {
namespace log {

class LogCustomer {
 public:
  LogCustomer(bool async = false, bool through_mode = false);
  virtual ~LogCustomer(){
    Deinitialize();
  }
  void Deinitialize();
  virtual void Consume(PlatformLogMessagePtr const & message) = 0;
#if USE_BUILTIN_LOG_SYS
  void ConsumeMsg(PlatformLogMessagePtr const & message) ;
  bool through_mode_;
#endif
 protected:
  virtual void flush() {};
  bool is_running_ = false;
#if USE_BUILTIN_LOG_SYS
  void proccess();
  std::list<PlatformLogMessagePtr /*const*/> messages_;
  std::thread proccessor;
  std::mutex messages_mutex_;
  sem_t sem_for_new_message_;
  bool async_;
#endif
};

}  // namespace log
}  // namespace hanhai

#endif // PLATFORM_LOG_CUSTOMER_H